import 'package:flutter/material.dart';
import 'package:nuntium_app/core/core.dart';
import 'package:nuntium_app/features/features.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  const WebViewPage({
    Key? key,
    required WebViewPageParams params,
  })  : _params = params,
        super(key: key);

  final WebViewPageParams _params;

  @override
  State<WebViewPage> createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  late WebViewController _controller;
  double _progress = 0.0;
  bool _isloading = true;

  void _setProgress(double progress) {
    if (mounted) {
      setState(() {
        _progress = progress;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Colors.transparent)
      ..platform.clearCache()
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            double _currentProgress = progress / 100;
            _setProgress(_currentProgress);

            if (_currentProgress == 1.0) {
              setState(() {
                _isloading = false;
              });
            }
            debugPrint('Web view is Loading (porgress: $progress% )');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            setState(() {
              _progress = 1.0;
              _isloading = false;
            });
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
Page resource error:
  code: ${error.errorCode}
  description: ${error.description}
  errorType: ${error.errorType}
  isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith(UrlConstant.baseUrl)) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse(widget._params.url));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _isloading
            ? Center(
                child: Text(
                  'Tolong Tunggu\n${(_progress * 100).toInt()} %',
                  style: AppTextStyle.medium.copyWith(
                    fontSize: AppFontSize.large,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            : WebViewWidget(
                controller: _controller,
                layoutDirection: TextDirection.ltr,
              ),
      ),
    );
  }
}
