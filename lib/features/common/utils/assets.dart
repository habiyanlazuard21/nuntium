class AppIcon {
  static const _baseIconAssets = './assets/icons';
  static const logo = '$_baseIconAssets/logo.png';
  static const facebook = '$_baseIconAssets/ic_facebook.png';
  static const google = '$_baseIconAssets/ic_google.png';
  static const homePurple = '$_baseIconAssets/ic_home_purple_prime.svg';
  static const homeGrey = '$_baseIconAssets/ic_home_grey_light.svg';
  static const appPurple = '$_baseIconAssets/ic_app_purple.svg';
  static const appGrey = '$_baseIconAssets/ic_app_grey.svg';
  static const bookmarkPurple = '$_baseIconAssets/ic_bookmark_purple.svg';
  static const bookmarkGrey = '$_baseIconAssets/ic_bookmark_grey.svg';
  static const userPurple = '$_baseIconAssets/ic_user_purple.svg';
  static const usergrey = '$_baseIconAssets/ic_user_grey.svg';
  static const close = '$_baseIconAssets/ic_close.png';
  static const success = '$_baseIconAssets/ic_success.png';
  static const error = '$_baseIconAssets/ic_warning.png';
}

class AppIllustrations {
  static const _baseIllustrationAssets = './assets/illustrations';
  static const dummy = '$_baseIllustrationAssets/ill_dummy.jpg';
}

class AppImageAssets {
  static const _baseImageAssets = './assets/images';
  static const onBoardingImage = '$_baseImageAssets/backgrounds.png';
  static const shakingHand = '$_baseImageAssets/hand.png';
  static const lineBottom = '$_baseImageAssets/line_bottom.png';
  static const lineBottomGrey = '$_baseImageAssets/line_bottom_grey.png';
  static const drawing = '$_baseImageAssets/img_book.png';
  static const profile = '$_baseImageAssets/profile.png';
}
