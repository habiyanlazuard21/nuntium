class PagePath {
  static const splash = '/';
  static const onBoarding = '/onBoarding';
  static const welcome = '/welcome';
  static const signIn = '/login';
  static const signUp = '/register';
  static const bottomNavBar = '/navigation';
  static const forgotPassword = '/forgotPassword';
  static const verification = '/verification';
  static const newPassword = '/newPassword';
  static const selectedCategory = '/selectedCategory';
  static const profile = '/profile';
  static const language = '/language';
  static const changedPassword = '/changedPassword';
  static const privacy = '/privacy';
  static const termAndConditions = '/termAndConditions';
  static const webViewPage = '/webView';
}
