import 'package:flutter/material.dart';
import 'package:nuntium_app/features/profile/presentation/pages/privacy_page.dart';
import 'package:nuntium_app/features/profile/presentation/pages/term_and_condition_page.dart';
import 'package:nuntium_app/features/web_view/presentation/pages/web_view_page.dart';
import 'package:nuntium_app/features/web_view/presentation/params/web_view_page_params.dart';

import '../../../lib.dart';

class PageRouter {
  Route<dynamic>? getRoute(
    RouteSettings settings,
  ) {
    switch (settings.name) {
      /* splash */
      //-------------------------------------------------------
      case PagePath.splash:
        return _buildRouter(
          settings: settings,
          builder: (args) => const SplashScreen(),
        );
      //------------------------------------------------------

      /* on Boaring */
      //-------------------------------------------------------
      case PagePath.onBoarding:
        return _buildRouter(
          settings: settings,
          builder: (args) => const OnBoardingScreen(),
        );
      //------------------------------------------------------

      /* Welcome */
      //-------------------------------------------------------
      case PagePath.welcome:
        return _buildRouter(
          settings: settings,
          builder: (args) => const WelcomeScreen(),
        );
      //------------------------------------------------------

      /* Login */
      //-------------------------------------------------------
      case PagePath.signIn:
        return _buildRouter(
          settings: settings,
          builder: (args) => const LoginScreen(),
        );
      //------------------------------------------------------

      /* Sign Up */
      //-------------------------------------------------------
      case PagePath.signUp:
        return _buildRouter(
          settings: settings,
          builder: (args) => const SignUpScreen(),
        );
      //------------------------------------------------------

      /* Forgot Password */
      //-------------------------------------------------------
      case PagePath.forgotPassword:
        return _buildRouter(
          settings: settings,
          builder: (args) => const ForgotPassword(),
        );
      //------------------------------------------------------

      /* Verification */
      //-------------------------------------------------------
      case PagePath.verification:
        return _buildRouter(
          settings: settings,
          builder: (args) => const VerificationPages(),
        );
      //------------------------------------------------------

      /* New Password */
      //-------------------------------------------------------
      case PagePath.newPassword:
        return _buildRouter(
          settings: settings,
          builder: (args) => const NewPassword(),
        );
      //------------------------------------------------------

      // /* Bottom Navigation */
      // //-------------------------------------------------------
      case PagePath.bottomNavBar:
        return _buildRouter(
          settings: settings,
          builder: (args) => BottomNavigation(selectedIndex: args as int? ?? 0),
        );
      //------------------------------------------------------

      /* Category */
      //-------------------------------------------------------
      case PagePath.selectedCategory:
        return _buildRouter(
          settings: settings,
          builder: (args) => SelectedCategoryPages(
            params: args as SelectedCategoryParams? ?? SelectedCategoryParams(),
          ),
        );
      //------------------------------------------------------

      // /* Detail Image Event */
      // //-------------------------------------------------------
      // case PagePath.detailImageEventPage:
      //   return _buildRouter(
      //     settings: settings,
      //     builder: (args) => DetailImageEventPage(
      //       params: args as DetailImageEventParams? ?? DetailImageEventParams(),
      //     ),
      //   );

      /* profile screen */
      //-------------------------------------------------------
      case PagePath.profile:
        return _buildRouter(
          settings: settings,
          builder: (args) => const ProfileScreen(),
        );
      //------------------------------------------------------

      /* bahasa pages */
      //-------------------------------------------------------
      case PagePath.language:
        return _buildRouter(
          settings: settings,
          builder: (args) => const LanguagePage(),
        );
      //------------------------------------------------------

      /* chaged password */
      //-------------------------------------------------------
      case PagePath.changedPassword:
        return _buildRouter(
          settings: settings,
          builder: (args) => const ChangedPasswordPage(),
        );
      //------------------------------------------------------

      /* privacy */
      //-------------------------------------------------------
      case PagePath.privacy:
        return _buildRouter(
          settings: settings,
          builder: (args) => const PrivacyPage(),
        );
      //------------------------------------------------------

      /* Term And Conditions */
      //-------------------------------------------------------
      case PagePath.termAndConditions:
        return _buildRouter(
          settings: settings,
          builder: (args) => const TermAndConditionPage(),
        );
      //------------------------------------------------------

      /* Web View */
      //-------------------------------------------------------
      case PagePath.webViewPage:
        return _buildRouter(
          settings: settings,
          builder: (args) => WebViewPage(
              params: args as WebViewPageParams? ?? WebViewPageParams()),
        );
      //------------------------------------------------------

      default:
        return null;
    }
  }

  Route<dynamic> _buildRouter({
    required RouteSettings settings,
    required Widget Function(Object? arguments) builder,
  }) {
    return MaterialPageRoute(
      settings: settings,
      builder: (_) => builder(settings.arguments),
    );
  }
}
