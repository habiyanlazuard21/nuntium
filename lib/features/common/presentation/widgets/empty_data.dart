import 'package:flutter/material.dart';

import '../../../../lib.dart';

class EmptyData extends StatelessWidget {
  final double fontSize;
  const EmptyData({Key? key, this.fontSize = AppFontSize.large})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Keyword",
        style: AppTextStyle.semiBold.copyWith(
          fontSize: fontSize,
        ),
      ),
    );
  }
}
