import 'package:flutter/material.dart';

import '../../../../lib.dart';

class Indicator extends StatelessWidget {
  final Color _borderColor;
  final bool _isActive;
  final double _paddingHorizontal;
  final double _activeSize;
  final double _inactiveSize;
  final Color _activeColor;
  final Color _inactiveColor;

  const Indicator({
    Key? key,
    Color borderColor = Colors.transparent,
    required bool isActive,
    double activeSize = AppIndicatorSize.active,
    double inactiveSize = AppIndicatorSize.inactive,
    double paddingHorizontal = AppGap.extraSmall,
    Color activeColor = AppColors.purplePrime,
    Color inactiveColor = const Color(0xFFE1D3C9),
  })  : _borderColor = borderColor,
        _paddingHorizontal = paddingHorizontal,
        _activeSize = activeSize,
        _inactiveSize = inactiveSize,
        _isActive = isActive,
        _activeColor = activeColor,
        _inactiveColor = inactiveColor,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: _paddingHorizontal,
      ),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 250),
        height: _isActive ? _activeSize : _inactiveSize,
        width: _isActive ? _activeSize : _inactiveSize,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: _isActive ? _activeColor : _inactiveColor,
          // borderRadius: BorderRadius.circular(AppBorderRadius.medium),
          border: Border.all(
            color: _borderColor,
            width: 0.5,
          ),
        ),
      ),
    );
  }
}

class Pagination extends StatelessWidget {
  const Pagination({
    Key? key,
    required int length,
    required int currentIndex,
    Color borderColor = Colors.transparent,
  })  : _length = length,
        _currentIndex = currentIndex,
        _borderColor = borderColor,
        super(key: key);

  final int _length;
  final int _currentIndex;
  final Color _borderColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ...List.generate(_length, (index) {
          return Indicator(
            isActive: _currentIndex == index,
            borderColor: _borderColor,
          );
        })
      ],
    );
  }
}
