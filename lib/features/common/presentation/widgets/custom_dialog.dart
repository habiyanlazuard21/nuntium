import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart' as context;

import '../../../../lib.dart';

extension CustomDialog on context.BuildContext {
  void successDialog({
    required String message,
    Function()? onPressed,
    bool isShowButton = false,
    TextStyle? style,
  }) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        bool isClose = false;

        void close(bool close) {
          if (close) {
            Navigator.pop(ctx);
            if (onPressed != null) onPressed();
          }
        }

        Timer.periodic(const Duration(seconds: 2), (timer) {
          timer.cancel();
          isClose = !isClose;
          close(isClose);
        });

        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(ctx).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(ctx).getMediaQueryWidth() - 430) / 2) +
                    AppGap.extraLarge * 2
                : AppGap.extraLarge * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          isClose = !isClose;
                          close(isClose);
                        },
                        child: Image.asset(
                          AppIcon.close,
                          width: AppIconSize.large,
                          height: AppIconSize.large,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Center(
                      child: Image.asset(
                        AppIcon.success,
                        width: AppIconSize.dialog,
                        height: AppIconSize.dialog,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              const Gap(height: AppGap.medium),
              Text(
                message,
                textAlign: TextAlign.center,
                maxLines: 15,
                overflow: TextOverflow.ellipsis,
                style: style ?? AppTextStyle.semiBold,
              ),
              Visibility(
                visible: isShowButton,
                child: Padding(
                  padding: const EdgeInsets.only(top: AppGap.large),
                  child: ButtonPrimary(
                    "OK",
                    width: 150,
                    borderRadius: AppBorderRadius.normal,
                    fontWeight: AppFontWeight.regular,
                    onPressed: () {
                      isClose = !isClose;
                      close(isClose);
                    },
                  ),
                ),
              ),
              const Gap(height: AppGap.normal),
            ],
          ),
        );
      },
    );
  }

  void errorDialog({required String message}) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext context) {
        bool isClose = false;

        void close(bool close) {
          if (close) Navigator.pop(this);
        }

        Timer.periodic(const Duration(seconds: 2), (timer) {
          timer.cancel();
          isClose = !isClose;
          close(isClose);
        });

        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(this).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(this).getMediaQueryWidth() - 430) / 2) +
                    AppGap.extraLarge * 2
                : AppGap.extraLarge * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          isClose = !isClose;
                          close(isClose);
                        },
                        child: Image.asset(
                          AppIcon.close,
                          width: AppIconSize.large,
                          height: AppIconSize.large,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Center(
                      child: Image.asset(
                        AppIcon.error,
                        width: AppIconSize.dialog,
                        height: AppIconSize.dialog,
                        color: Colors.red,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              const Gap(height: AppGap.medium),
              Text(
                message,
                textAlign: TextAlign.center,
                maxLines: 8,
                overflow: TextOverflow.ellipsis,
                style: AppTextStyle.semiBold,
              ),
              const Gap(height: AppGap.normal),
            ],
          ),
        );
      },
    );
  }

  void loadingDialog({Key? key, bool isDownload = false}) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (context) {
        final responsive = ResponsiveUtils(context);
        return Dialog(
          backgroundColor: AppColors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppBorderRadius.large),
          ),
          insetPadding: EdgeInsets.symmetric(
            horizontal: (responsive.getMediaQueryWidth() - 200) / 2,
          ),
          child: SizedBox(
            height: 150,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const LoadingIndicator(),
                Visibility(
                  visible: isDownload,
                  child: const Padding(
                    padding: EdgeInsets.only(top: AppGap.normal),
                    child: Text("Downloading..."),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  // void downloadSuccess() {
  //   showDialog(
  //     context: this,
  //     builder: (context) {
  //       final responsive = ResponsiveUtils(context);
  //       return Dialog(
  //         backgroundColor: AppColors.white,
  //         elevation: 0,
  //         shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(AppBorderRadius.large),
  //         ),
  //         insetPadding: EdgeInsets.symmetric(
  //           horizontal: (responsive.getMediaQueryWidth() - 300) / 2,
  //         ),
  //         child: SizedBox(
  //           height: 250,
  //           child: Column(
  //             mainAxisSize: MainAxisSize.min,
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: [
  //               Image.asset(AppIcon.checklist, height: 86),
  //               const Gap(height: AppGap.normal),
  //               Text(
  //                 "Success!",
  //                 style: AppTextStyle.bold.copyWith(
  //                   fontSize: AppFontSize.large,
  //                 ),
  //               ),
  //               Text(
  //                 "Photo has been saved to gallery",
  //                 style: AppTextStyle.medium.copyWith(
  //                   color: Colors.black54,
  //                   fontSize: AppFontSize.small,
  //                 ),
  //               ),
  //               const Gap(height: AppGap.normal),
  //               Padding(
  //                 padding:
  //                     const EdgeInsets.symmetric(horizontal: AppGap.medium),
  //                 child: ButtonPrimary(
  //                   "OK",
  //                   height: AppButtonSize.normal - 4,
  //                   width: double.infinity,
  //                   buttonColor: AppColors.green,
  //                   borderRadius: AppBorderRadius.small,
  //                   onPressed: () {
  //                     Navigator.pop(this);
  //                   },
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  // void accesDialog({required dynamic Function() onPressed}) {
  //   showDialog(
  //     context: this,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         insetPadding: EdgeInsets.symmetric(
  //           horizontal: ResponsiveUtils(this).getMediaQueryWidth() > 430
  //               ? ((ResponsiveUtils(this).getMediaQueryWidth() - 430) / 2) + 14
  //               : 14,
  //         ),
  //         content: Column(
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.min,
  //           children: [
  //             Text(
  //               // "Maaf Akses ini hanya untuk member Fotogrit. Silahkan Login atau Sign Up untuk melanjutkan proses.",
  //               "Sorry, this access is only for Fotogrit members. Please Log In or Sign Up to continue the process.",
  //               textAlign: TextAlign.center,
  //               maxLines: 5,
  //               style:
  //                   AppTextStyle.regular.copyWith(fontSize: AppFontSize.small),
  //             ),
  //             const Gap(height: AppGap.medium),
  //             ButtonPrimary(
  //               "Login / Sign Up",
  //               height: AppGap.normal * 3,
  //               width: double.infinity,
  //               onPressed: onPressed,
  //               fontWeight: AppFontWeight.semiBold,
  //             ),
  //           ],
  //         ),
  //       );
  //     },
  //   );
  // }

  void permissionDialog({required dynamic Function() onPressed}) {
    showDialog(
      context: this,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Please Allow Access To Photos To Upload Photos From Your Library!",
                style: AppTextStyle.semiBold
                    .copyWith(fontSize: AppFontSize.normal),
              ),
              const Gap(height: AppGap.medium),
              ButtonPrimary(
                "Go To App Settings",
                height: AppGap.normal * 3,
                width: double.infinity,
                onPressed: onPressed,
              ),
            ],
          ),
        );
      },
    );
  }

  void confirmationDialog({
    required String message,
    required Function() onPressed,
  }) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(this).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(this).getMediaQueryWidth() - 430) / 2) +
                    AppGap.medium * 2
                : AppGap.medium * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Confirmation",
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: AppTextStyle.semiBold
                    .copyWith(fontSize: AppFontSize.medium),
              ),
              const Gap(height: AppGap.medium),
              Text(
                message,
                textAlign: TextAlign.center,
                maxLines: 7,
                overflow: TextOverflow.ellipsis,
                style:
                    AppTextStyle.regular.copyWith(fontSize: AppFontSize.small),
              ),
              const Gap(height: AppGap.large),
              Row(
                children: [
                  Expanded(
                    child: ButtonPrimary(
                      "Cancel",
                      borderColor: AppColors.purplePrime,
                      buttonColor: AppColors.white,
                      labelColor: AppColors.purplePrime,
                      fontWeight: AppFontWeight.regular,
                      borderRadius: AppBorderRadius.normal,
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  const Gap(),
                  Expanded(
                    child: ButtonPrimary(
                      "OK",
                      borderRadius: AppBorderRadius.normal,
                      fontWeight: AppFontWeight.regular,
                      onPressed: onPressed,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  void attentionDialog({
    required String message,
    required Function() onPressed,
  }) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(this).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(this).getMediaQueryWidth() - 430) / 2) +
                    AppGap.medium * 2
                : AppGap.medium * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Attention!",
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: AppTextStyle.semiBold
                    .copyWith(fontSize: AppFontSize.medium),
              ),
              const Gap(height: AppGap.large),
              Image.asset(
                AppIcon.error,
                width: AppIconSize.dialog,
                height: AppIconSize.dialog,
                color: Colors.red,
                fit: BoxFit.contain,
              ),
              const Gap(height: AppGap.medium),
              Text(
                message,
                textAlign: TextAlign.center,
                maxLines: 10,
                overflow: TextOverflow.ellipsis,
                style:
                    AppTextStyle.regular.copyWith(fontSize: AppFontSize.small),
              ),
              const Gap(height: AppGap.large),
              Row(
                children: [
                  Expanded(
                    child: ButtonPrimary(
                      "Cancel",
                      borderColor: AppColors.purplePrime,
                      buttonColor: AppColors.white,
                      labelColor: AppColors.purplePrime,
                      fontWeight: AppFontWeight.regular,
                      borderRadius: AppBorderRadius.normal,
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  const Gap(),
                  Expanded(
                    child: ButtonPrimary(
                      "OK",
                      borderRadius: AppBorderRadius.normal,
                      fontWeight: AppFontWeight.regular,
                      onPressed: onPressed,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  void requestEvent({
    required String title,
    required TextFieldEntity textFieldEntity,
    required Function() onPressed,
  }) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(this).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(this).getMediaQueryWidth() - 430) / 2) +
                    AppGap.medium * 2
                : AppGap.medium * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: AppTextStyle.semiBold.copyWith(
                  fontSize: AppFontSize.medium,
                ),
              ),
              const Gap(height: AppGap.medium),
              CustomTextFormFieldBordered(
                textFieldEntity: textFieldEntity,
                validator: textFieldEntity.validator,
              ),
              const Gap(height: AppGap.large),
              Row(
                children: [
                  Expanded(
                    child: ButtonPrimary(
                      "Cancel",
                      borderColor: AppColors.purplePrime,
                      buttonColor: AppColors.white,
                      labelColor: AppColors.purplePrime,
                      fontWeight: AppFontWeight.regular,
                      borderRadius: AppBorderRadius.normal,
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  const Gap(),
                  Expanded(
                    child: ButtonPrimary(
                      "OK",
                      borderRadius: AppBorderRadius.normal,
                      fontWeight: AppFontWeight.regular,
                      onPressed: onPressed,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  void sessionExpiredDialog({
    required String message,
    required Function() onPressed,
    TextStyle? style,
  }) {
    showDialog(
      context: this,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return AlertDialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveUtils(ctx).getMediaQueryWidth() > 430
                ? ((ResponsiveUtils(ctx).getMediaQueryWidth() - 430) / 2) +
                    AppGap.extraLarge * 2
                : AppGap.extraLarge * 2,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(ctx);
                          onPressed();
                        },
                        child: Image.asset(
                          AppIcon.close,
                          width: AppIconSize.large,
                          height: AppIconSize.large,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Center(
                      child: Image.asset(
                        AppIcon.error,
                        width: AppIconSize.dialog,
                        height: AppIconSize.dialog,
                        color: Colors.red,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              const Gap(height: AppGap.medium),
              Text(
                message,
                textAlign: TextAlign.center,
                maxLines: 15,
                overflow: TextOverflow.ellipsis,
                style: style ?? AppTextStyle.semiBold,
              ),
              Padding(
                padding: const EdgeInsets.only(top: AppGap.large),
                child: ButtonPrimary(
                  "OK",
                  width: 150,
                  borderRadius: AppBorderRadius.normal,
                  fontWeight: AppFontWeight.regular,
                  onPressed: () {
                    Navigator.pop(ctx);
                    onPressed();
                  },
                ),
              ),
              const Gap(height: AppGap.normal),
            ],
          ),
        );
      },
    );
  }
}

class BaseDialogCard extends StatelessWidget {
  const BaseDialogCard({Key? key, required List<Widget> children})
      : _children = children,
        super(key: key);

  final List<Widget> _children;

  @override
  Widget build(BuildContext context) {
    final responsive = ResponsiveUtils(context);
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(
        textScaleFactor: 1,
      ),
      child: Dialog(
        backgroundColor: AppColors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppBorderRadius.large),
        ),
        insetPadding: EdgeInsets.symmetric(
          horizontal: responsive.getResponsiveSize(AppGap.big),
        ),
        child: ColumnPadding(
          padding: EdgeInsets.symmetric(
            horizontal: responsive.getResponsiveSize(AppGap.large),
            vertical: responsive.getResponsiveSize(AppGap.extraLarge),
          ),
          mainAxisSize: MainAxisSize.min,
          children: _children,
        ),
      ),
    );
  }
}
