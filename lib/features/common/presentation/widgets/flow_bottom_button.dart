import 'package:flutter/material.dart';

import '../../../../lib.dart';

class FlowBottomButton extends StatelessWidget {
  final String _title;
  final int _value;
  final String _labelButton;
  final Function() _onPressed;
  final bool _isDisable;
  final bool _isVisible;
  const FlowBottomButton({
    Key? key,
    required String title,
    required int value,
    required String labelButton,
    required Function() onPressed,
    bool isDisable = false,
    required bool isVisible,
  })  : _title = title,
        _value = value,
        _labelButton = labelButton,
        _onPressed = onPressed,
        _isDisable = isDisable,
        _isVisible = isVisible,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: AppGap.medium,
      left: AppGap.extraLarge,
      right: AppGap.extraLarge,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(AppBorderRadius.small),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 4,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: RowPadding(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          padding: const EdgeInsets.symmetric(
            horizontal: AppGap.medium,
            vertical: AppGap.normal,
          ),
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _title,
                  style: AppTextStyle.medium.copyWith(
                    fontSize: AppFontSize.small,
                  ),
                ),
                Text(
                  "dasd",
                  style: AppTextStyle.semiBold.copyWith(
                    color: AppColors.purplePrime,
                  ),
                ),
              ],
            ),
            Visibility(
              visible: _isVisible,
              replacement: const ButtonLoading(
                height: AppGap.normal * 3,
                buttonColor: AppColors.blackPrimary,
              ),
              child: ButtonPrimary(
                _labelButton,
                height: AppGap.normal * 3,
                buttonColor: AppColors.blackPrimary,
                fontWeight: AppFontWeight.regular,
                isButtonDisabled: _isDisable,
                fontSize: AppFontSize.small,
                onPressed: _onPressed,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NewFlowBottomButton extends StatelessWidget {
  final String _title;
  final int _value;
  final String _labelButton;
  final Function() _onPressed;
  final bool _isDisable;
  final bool _isVisible;
  const NewFlowBottomButton({
    Key? key,
    required String title,
    required int value,
    required String labelButton,
    required Function() onPressed,
    bool isDisable = false,
    required bool isVisible,
  })  : _title = title,
        _value = value,
        _labelButton = labelButton,
        _onPressed = onPressed,
        _isDisable = isDisable,
        _isVisible = isVisible,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: AppGap.medium,
      left: AppGap.extraLarge,
      right: AppGap.extraLarge,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(AppBorderRadius.large),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 4,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        height: AppGap.large * 4,
        child: RowPadding(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          padding: const EdgeInsets.symmetric(
            horizontal: AppGap.medium,
          ),
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _title,
                    style: AppTextStyle.semiBold.copyWith(fontSize: 13),
                  ),
                  const Gap(height: AppGap.small / 2),
                  Text(
                    "",
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                    style: AppTextStyle.semiBold.copyWith(
                      fontSize: AppFontSize.large,
                      color: AppColors.purplePrime,
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: _isVisible,
              replacement: const ButtonLoading(
                height: AppGap.normal * 3,
                buttonColor: AppColors.blackPrimary,
              ),
              child: ButtonPrimary(
                _labelButton,
                height: AppGap.normal * 3,
                buttonColor: AppColors.purplePrime,
                labelColor: const Color(0xFFFEE8D1),
                fontWeight: AppFontWeight.regular,
                isButtonDisabled: _isDisable,
                borderRadius: AppBorderRadius.normal,
                paddingHorizontal: AppGap.medium * 2,
                onPressed: _onPressed,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
