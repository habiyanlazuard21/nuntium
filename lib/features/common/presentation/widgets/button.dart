import 'package:flutter/material.dart';

import '../../../../lib.dart';

class ButtonPrimary extends StatelessWidget {
  const ButtonPrimary(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    double? elevation,
    TextDecoration? textDecoration,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _elevation = elevation,
        _textDecoration = textDecoration,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final TextDecoration? _textDecoration;
  final double? _elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: _elevation,
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Text(
          _label,
          style: TextStyle(
            fontSize: _fontSize,
            color: _labelColor,
            fontWeight: _fontWeight,
            decoration: _textDecoration,
          ),
          textAlign: _textAlign,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}

class ButtonLoading extends StatelessWidget {
  const ButtonLoading({
    Key? key,
    Color buttonColor = AppColors.purplePrime,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.small / 2,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    Color loadingColor = AppColors.purplePrime,
  })  : _buttonColor = buttonColor,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _loadingColor = loadingColor,
        super(key: key);

  final Color _buttonColor;
  final Color _borderColor;
  final double _borderWidth;
  final double _borderRadius;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final Color _loadingColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          // backgroundColor: _buttonColor,
          disabledBackgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: null,
        child: SizedBox(
          height: _height ??
              ResponsiveUtils(context).getResponsiveSize(
                AppButtonSize.normal,
              ),
          width: _height ??
              ResponsiveUtils(context).getResponsiveSize(
                AppButtonSize.normal,
              ),
          child: LoadingIndicator(color: _loadingColor),
        ),
      ),
    );
  }
}

class ButtonPrimaryBack extends StatelessWidget {
  const ButtonPrimaryBack(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(left: AppGap.normal),
              child: Icon(
                Icons.arrow_back_ios,
                size: AppIconSize.normal,
                color: _labelColor,
              ),
            ),
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}

class ButtonPrimaryContinue extends StatelessWidget {
  const ButtonPrimaryContinue(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: Icon(
                Icons.arrow_forward_ios,
                size: AppIconSize.normal,
                color: _labelColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtonWithArrow extends StatelessWidget {
  const CustomButtonWithArrow(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    bool isShow = false,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _isShow = isShow,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final bool _isShow;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Icon(
              _isShow
                  ? Icons.keyboard_arrow_up_outlined
                  : Icons.keyboard_arrow_down_outlined,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtonWithIconImage extends StatelessWidget {
  const CustomButtonWithIconImage({
    Key? key,
    required String label,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = FontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = Colors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = 8,
    double fontSize = 16,
    double paddingVertical = 8,
    double paddingHorizontal = 16,
    double? height,
    double? width,
    required String icon,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _icon = icon,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final String _icon;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          disabledForegroundColor: Colors.white,
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Stack(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(left: 16),
              child: CustomImageWrapper(
                image: _icon,
                width: 24,
                height: 24,
                isNetworkImage: false,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                _label,
                style: TextStyle(
                  fontSize: _fontSize,
                  color: _labelColor,
                  fontWeight: _fontWeight,
                ),
                textAlign: _textAlign,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NewCustomButtonWithArrow extends StatelessWidget {
  const NewCustomButtonWithArrow(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    bool isShow = false,
    bool isChangedIcon = false,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: _labelColor,
              size: _fontSize,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtonSignOut extends StatelessWidget {
  const CustomButtonSignOut(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    bool isShow = false,
    bool isChangedIcon = false,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Icon(
              Icons.logout,
              color: _labelColor,
              size: _fontSize,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtonNotification extends StatefulWidget {
  const CustomButtonNotification(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    bool isShow = false,
    bool isChangedIcon = false,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  State<CustomButtonNotification> createState() =>
      _CustomButtonNotificationState();
}

class _CustomButtonNotificationState extends State<CustomButtonNotification> {
  bool _isEnabled = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget._width,
      height: widget._height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: widget._buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: widget._paddingVertical,
            horizontal: widget._paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(
              color: widget._borderColor, width: widget._borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              widget._borderRadius,
            ),
          ),
        ),
        onPressed: widget._isButtonDisabled ? null : widget._onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget._label,
              style: TextStyle(
                fontSize: widget._fontSize,
                color: widget._labelColor,
                fontWeight: widget._fontWeight,
              ),
              textAlign: widget._textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _isEnabled = !_isEnabled;
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  color: _isEnabled
                      ? AppColors.purplePrimary
                      : AppColors.greyPrimary,
                  borderRadius:
                      BorderRadiusDirectional.circular(AppBorderRadius.medium),
                ),
                width: 40,
                height: 25,
                child: Stack(
                  children: [
                    _isEnabled
                        ? Positioned(
                            right: 2,
                            top: 2,
                            bottom: 2,
                            child: Container(
                              width: 20,
                              height: 20,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColors.white,
                              ),
                            ),
                          )
                        : Positioned(
                            left: 2,
                            top: 2,
                            bottom: 2,
                            child: Container(
                              width: 20,
                              height: 20,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColors.white,
                              ),
                            ),
                          )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtonCeklis extends StatefulWidget {
  const CustomButtonCeklis(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.purplePrime,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    bool isShow = false,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _isShow = isShow,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final bool _isShow;

  @override
  State<CustomButtonCeklis> createState() => _CustomButtonCeklisState();
}

class _CustomButtonCeklisState extends State<CustomButtonCeklis> {
  bool _isEnabled = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget._width,
      height: widget._height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: widget._buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: widget._paddingVertical,
            horizontal: widget._paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(
              color: widget._borderColor, width: widget._borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              widget._borderRadius,
            ),
          ),
        ),
        onPressed: widget._isButtonDisabled ? null : widget._onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget._label,
              style: TextStyle(
                fontSize: widget._fontSize,
                color: widget._labelColor,
                fontWeight: widget._fontWeight,
              ),
              textAlign: widget._textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            widget._isShow
                ? Icon(
                    Icons.done,
                    size: widget._fontSize,
                    color: widget._labelColor,
                  )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
