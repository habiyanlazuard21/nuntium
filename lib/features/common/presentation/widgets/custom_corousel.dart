import 'dart:async';

import 'package:flutter/material.dart';

import '../../../../lib.dart';

class CustomCarouselWithIndicator extends StatefulWidget {
  const CustomCarouselWithIndicator({
    Key? key,
    required int itemCount,
    required Widget Function(BuildContext, int) itemBuilder,
    Color indicatorBorderColor = Colors.transparent,
    bool autoAnimate = false,
    double height = 136,
    Duration indexChangeDuration = const Duration(milliseconds: 2500),
    Duration animateToPageDuration = const Duration(milliseconds: 1000),
    Curve animateToPageCurve = Curves.easeInCubic,
    Color backgroundColor = Colors.transparent,
    double radius = 0,
  })  : _height = height,
        _itemCount = itemCount,
        _itemBuilder = itemBuilder,
        _autoAnimate = autoAnimate,
        _indexChangeDuration = indexChangeDuration,
        _animateToPageDuration = animateToPageDuration,
        _animateToPageCurve = animateToPageCurve,
        _backgroundColor = backgroundColor,
        _indicatorBorderColor = indicatorBorderColor,
        _radius = radius,
        super(key: key);

  final int _itemCount;
  final double _height;
  final bool _autoAnimate;
  final Duration _indexChangeDuration;
  final Duration _animateToPageDuration;
  final Curve _animateToPageCurve;
  final Widget Function(BuildContext, int) _itemBuilder;
  final Color _backgroundColor;
  final Color _indicatorBorderColor;
  final double _radius;

  @override
  State<CustomCarouselWithIndicator> createState() =>
      _CustomCarouselWithIndicatorState();
}

class _CustomCarouselWithIndicatorState
    extends State<CustomCarouselWithIndicator> {
  final PageController _pageController = PageController();
  int _currentPage = 0;
  Timer? _timer;

  void _startTimer(int len, PageController pageController) {
    _timer = Timer.periodic(widget._indexChangeDuration, (Timer timer) {
      if (_currentPage < len - 1) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      if (pageController.hasClients) {
        pageController.animateToPage(
          _currentPage,
          duration: widget._animateToPageDuration,
          curve: widget._animateToPageCurve,
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget._autoAnimate) {
      _timer?.cancel();
      _startTimer(widget._itemCount, _pageController);
    }
  }

  /* Reset all timer & index */
  @override
  void dispose() {
    if (_timer?.isActive ?? false) {
      if (mounted) {
        _pageController.dispose();
        _timer?.cancel();
        _currentPage = 0;
      }
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: widget._height,
      decoration: BoxDecoration(
        color: widget._backgroundColor,
        borderRadius: BorderRadius.circular(widget._radius),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(.5),
            offset: const Offset(1, 2.5),
            blurRadius: 8,
          ),
        ],
      ),
      child: Stack(
        children: [
          PageView.builder(
            itemCount: widget._itemCount,
            controller: _pageController,
            onPageChanged: (page) {
              setState(() {
                _currentPage = page;
              });
              if (widget._autoAnimate) {
                _timer?.cancel();
                _startTimer(
                  widget._itemCount,
                  _pageController,
                );
              }
            },
            itemBuilder: (context, index) {
              return widget._itemBuilder(context, index);
            },
          ),
          Visibility(
            visible: widget._itemCount > 1,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: 20,
                child: Pagination(
                  borderColor: widget._indicatorBorderColor,
                  length: widget._itemCount,
                  currentIndex: _currentPage,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
