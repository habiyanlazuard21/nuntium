import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomScrollViewWrapper extends StatelessWidget {
  const CustomScrollViewWrapper({
    Key? key,
    required List<Widget> slivers,
    bool isScrollable = true,
    Function()? onTap,
    ScrollController? controller,
  })  : _slivers = slivers,
        _onTap = onTap,
        _isScrollable = isScrollable,
        _controller = controller,
        super(key: key);

  final List<Widget> _slivers;
  final bool _isScrollable;
  final Function()? _onTap;
  final ScrollController? _controller;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: kIsWeb
          ? null
          : () {
              FocusScope.of(context).unfocus();
              if (_onTap != null) {
                _onTap;
              }
            },
      child: CustomScrollView(
        controller: _controller,
        physics: _isScrollable
            ? const AlwaysScrollableScrollPhysics()
            : const NeverScrollableScrollPhysics(),
        slivers: _slivers,
      ),
    );
  }
}

class CustomSingleChildScrollViewWrapper extends StatelessWidget {
  const CustomSingleChildScrollViewWrapper({
    Key? key,
    Function()? onTap,
    required Widget child,
    ScrollPhysics? physics,
  })  : _onTap = onTap,
        _child = child,
        _physics = physics,
        super(key: key);

  final Function()? _onTap;
  final Widget _child;
  final ScrollPhysics? _physics;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: kIsWeb
          ? null
          : () {
              FocusScope.of(context).unfocus();
              if (_onTap != null) {
                _onTap;
              }
            },
      splashColor: Colors.transparent,
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          physics: _physics,
          child: _child,
        ),
      ),
    );
  }
}
