import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuntium_app/injection.dart';

import '../../../../lib.dart';

class SelectedCategoryPages extends StatefulWidget {
  const SelectedCategoryPages({
    Key? key,
    required SelectedCategoryParams params,
  })  : _params = params,
        super(key: key);

  final SelectedCategoryParams _params;

  @override
  State<SelectedCategoryPages> createState() => _SelectedCategoryPagesState();
}

class _SelectedCategoryPagesState extends State<SelectedCategoryPages> {
  final List<ArticleEntity> listArticle = [];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<CategoryGeneralCubit>()
        ..getGeneralCategory(params: widget._params),
      child: BlocConsumer<CategoryGeneralCubit, CategoryGeneralState>(
        listener: (context, state) {
          if (state is CategoryGeneralError) {
            context.errorDialog(message: state.failure.message);
          }
        },
        builder: (context, state) {
          if (state is CategoryGeneralLoaded) {
            listArticle.clear();

            listArticle.addAll(state.generalList ?? []);
          } else if (state is CategoryGeneralError) {
            // listArticle.clear();
          }

          return SelectedCategoryWrapper(
            getListItem: listArticle,
            params: widget._params,
            isLoading: state is CategoryGeneralLoading && state.isLoading,
          );
        },
      ),
    );

    // return SelectedCategoryPageDummy(params: widget._params);
  }
}

class SelectedCategoryWrapper extends StatefulWidget {
  const SelectedCategoryWrapper({
    Key? key,
    required SelectedCategoryParams params,
    required List<ArticleEntity> getListItem,
    required bool isLoading,
  })  : _params = params,
        _isLoading = isLoading,
        _getListItem = getListItem,
        super(key: key);

  final SelectedCategoryParams _params;
  final bool _isLoading;
  final List<ArticleEntity> _getListItem;

  @override
  State<SelectedCategoryWrapper> createState() =>
      _SelectedCategoryWrapperState();
}

class _SelectedCategoryWrapperState extends State<SelectedCategoryWrapper> {
  final TextEditingController _searchCategoryController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      onWillPop: () => Navigator.pop(context),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              top: AppGap.extraLarge + 4,
            ),
            child: CustomAppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              title: widget._params.title,
              subtitle: widget._params.subtitle,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
            child: CustomTextFormFieldSearch(
              controller: _searchCategoryController,
              onPressed: () {
                FocusScope.of(context).unfocus();

                setState(() {
                  widget._params.search = _searchCategoryController.text.trim();
                });

                context
                    .read<CategoryGeneralCubit>()
                    .getGeneralCategory(params: widget._params);
              },
              onSubmitted: (value) {
                FocusScope.of(context).unfocus();

                setState(() {
                  widget._params.search = value.trim();
                });
                context
                    .read<CategoryGeneralCubit>()
                    .getGeneralCategory(params: widget._params);
              },
            ),
          ),
          const Gap(height: AppGap.extraLarge / 2),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                _searchCategoryController.text = '';
                setState(() {
                  widget._params.search = _searchCategoryController.text.trim();
                });

                context
                    .read<CategoryGeneralCubit>()
                    .getGeneralCategory(params: widget._params);
              },
              child: CustomScrollViewWrapper(
                slivers: [
                  /* Loading Indicator */
                  SliverVisibility(
                    visible: widget._isLoading,
                    sliver: const SliverFillRemaining(
                      hasScrollBody: false,
                      child: LoadingIndicator(),
                    ),
                  ),

                  /* List Article */
                  SliverVisibility(
                    visible:
                        widget._getListItem.isNotEmpty && !widget._isLoading,
                    sliver: SliverColumnPadding(
                      mainAxisSize: MainAxisSize.min,
                      padding: const EdgeInsets.only(
                        top: AppGap.normal,
                        left: AppGap.large,
                        right: AppGap.large,
                      ),
                      children: [
                        ...List.generate(
                            widget._getListItem.length > 15
                                ? 15
                                : widget._getListItem.length, (index) {
                          return _ItemCategory(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewPage(
                                    params: WebViewPageParams(
                                      url: widget._getListItem[index].url ?? '',
                                    ),
                                  ),
                                ),
                              );
                            },
                            categoryItemEntity: widget._getListItem[index],
                          );
                        }),
                      ],
                    ),
                  ),

                  /* Handle Empty data */
                  SliverVisibility(
                    visible: widget._getListItem.isEmpty && !widget._isLoading,
                    sliver: const SliverFillRemaining(
                      hasScrollBody: false,
                      child: EmptyData(),
                    ),
                  ),

                  /* bottom padding */
                  SliverVisibility(
                    visible:
                        widget._getListItem.isNotEmpty && !widget._isLoading,
                    sliver: SliverGap(
                      height:
                          ResponsiveUtils(context).getResponsiveBottomPadding(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _ItemCategory extends StatelessWidget {
  const _ItemCategory({
    Key? key,
    required ArticleEntity categoryItemEntity,
    required Function() onTap,
  })  : _categoryItemEntity = categoryItemEntity,
        _onTap = onTap,
        super(key: key);

  final ArticleEntity _categoryItemEntity;
  final Function() _onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.only(bottom: AppGap.medium),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(AppGap.normal),
        ),
        height: 96,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Visibility(
              visible: _categoryItemEntity.urlToImage?.isNotEmpty ?? false,
              child: CustomImageWrapper(
                image: _categoryItemEntity.urlToImage ?? '',
                width: 96,
                borderRadius: AppBorderRadius.normal,
                isNetworkImage: true,
              ),
            ),
            Visibility(
              visible: _categoryItemEntity.urlToImage?.isEmpty ?? true,
              child: const CustomImageWrapper(
                image: AppImageAssets.drawing,
                width: 96,
                borderRadius: AppBorderRadius.normal,
                isNetworkImage: false,
              ),
            ),
            Expanded(
              child: ColumnPadding(
                padding: const EdgeInsets.only(
                  left: AppGap.medium,
                  right: AppGap.normal,
                ),
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          _categoryItemEntity.author ?? '',
                          style: AppTextStyle.regular.copyWith(
                            color: AppColors.greyPrimary,
                            fontSize: AppFontSize.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: const Icon(
                          Icons.bookmark_border,
                          size: AppIconSize.medium,
                          color: AppColors.purpleDarker,
                        ),
                      )
                    ],
                  ),
                  const Gap(height: AppGap.small),
                  Text(
                    _categoryItemEntity.title ?? '',
                    style: AppTextStyle.regular.copyWith(
                      fontSize: AppFontSize.medium,
                      color: AppColors.blackPrimary,
                      overflow: TextOverflow.ellipsis,
                    ),
                    maxLines: 2,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
