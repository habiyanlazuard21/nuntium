import 'package:flutter/material.dart';

import '../../../../lib.dart';

class SelectedCategoryPageDummy extends StatefulWidget {
  const SelectedCategoryPageDummy({
    Key? key,
    required SelectedCategoryParams params,
  })  : _params = params,
        super(key: key);

  final SelectedCategoryParams _params;

  @override
  State<SelectedCategoryPageDummy> createState() =>
      _SelectedCategoryPageDummyState();
}

class _SelectedCategoryPageDummyState extends State<SelectedCategoryPageDummy> {
  final TextEditingController _searchCategoryController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      onWillPop: () => Navigator.pop(context),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              top: AppGap.extraLarge + 4,
            ),
            child: CustomAppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              title: widget._params.title,
              subtitle: widget._params.subtitle,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
            child: NewCustomTextFormFieldSearch(
              controller: _searchCategoryController,
              focusNode: FocusNode(
                canRequestFocus: true,
                skipTraversal: true,
              ),
              onSubmitted: (value) {
                setState(() {
                  widget._params.search = value.trim();
                });
              },
              onPressed: () {
                FocusScope.of(context).unfocus();

                setState(() {
                  widget._params.search = _searchCategoryController.text.trim();
                });
              },
            ),
          ),
          const Gap(height: AppGap.normal),
          Expanded(
            child: ListView.builder(
              itemCount: 5,
              shrinkWrap: true,
              padding: const EdgeInsets.only(
                top: AppGap.normal,
                left: AppGap.large,
                right: AppGap.large,
              ),
              itemBuilder: (context, index) {
                return Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(bottom: AppGap.medium),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AppGap.normal),
                  ),
                  height: 96,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const CustomImageWrapper(
                        image: AppImageAssets.drawing,
                        width: 96,
                        borderRadius: AppBorderRadius.normal,
                        isNetworkImage: false,
                      ),
                      Expanded(
                        child: ColumnPadding(
                          padding: const EdgeInsets.only(
                            left: AppGap.medium,
                            right: AppGap.normal,
                          ),
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "John Smith",
                                  style: AppTextStyle.regular.copyWith(
                                    color: AppColors.greyPrimary,
                                    fontSize: AppFontSize.normal,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {},
                                  child: const Icon(
                                    Icons.bookmark_border,
                                    size: AppIconSize.medium,
                                    color: AppColors.purpleDarker,
                                  ),
                                )
                              ],
                            ),
                            const Gap(height: AppGap.small),
                            Text(
                              "A Simple Trick For Creating Color Palettes Quickly",
                              style: AppTextStyle.regular.copyWith(
                                fontSize: AppFontSize.medium,
                                color: AppColors.blackPrimary,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
