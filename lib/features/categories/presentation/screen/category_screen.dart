import 'package:flutter/material.dart';
import 'package:nuntium_app/features/categories/data/entities/category_filter.dart';

import '../../../../lib.dart';

class CategoriScreen extends StatefulWidget {
  const CategoriScreen({
    Key? key,
    required CategoryScreenParams params,
  })  : _params = params,
        super(key: key);

  final CategoryScreenParams _params;

  @override
  State<CategoriScreen> createState() => _CategoriScreenState();
}

class _CategoriScreenState extends State<CategoriScreen> {
  final List<CategoryEntity> _getCategory = CategoryEntity.getCategory;
  final List<CategoryFilter> _getFiltercategory = CategoryFilter.getFilter;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
            ),
            child: const CustomAppBar(
              elevation: 0,
              title: "Categories",
              subtitle: "Thousands of articles in each category",
            ),
          ),
          const Gap(height: AppGap.extraLarge / 2),
          Expanded(
            child: GridView.builder(
              itemCount: _getCategory.length,
              padding: EdgeInsets.only(
                left: AppGap.large,
                right: AppGap.large,
                top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
                    AppGap.normal,
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
                mainAxisExtent: 72,
              ),
              itemBuilder: (context, index) {
                return ButtonPrimary(
                  _getCategory[index].category,
                  height: 72,
                  buttonColor: AppColors.white,
                  labelColor: AppColors.greyDarker,
                  fontWeight: AppFontWeight.regular,
                  borderRadius: AppBorderRadius.normal,
                  borderColor: AppColors.greyLighter,
                  elevation: 0,
                  onPressed: () {
                    setState(() {
                      widget._params.selectedIndex = index;
                    });
                    Navigator.pushNamed(
                      context,
                      PagePath.selectedCategory,
                      arguments: SelectedCategoryParams(
                        selectedIndex: index,
                        category: _getFiltercategory[index].category,
                        title: _getCategory[index].category,
                        subtitle: _getCategory[index].subtitle,
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
