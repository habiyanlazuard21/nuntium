import 'package:equatable/equatable.dart';

import '../../../../lib.dart';

abstract class CategoryGeneralState extends Equatable {
  const CategoryGeneralState();

  @override
  List<Object> get props => [];
}

class CategoryGeneralInitial extends CategoryGeneralState {}

class CategoryGeneralLoading extends CategoryGeneralState {
  final bool isLoading;

  const CategoryGeneralLoading(this.isLoading);
}

class CategoryGeneralLoaded extends CategoryGeneralState {
  final List<ArticleEntity>? generalList;

  const CategoryGeneralLoaded({this.generalList});
}

class CategoryGeneralError extends CategoryGeneralState {
  final Failure failure;

  const CategoryGeneralError(this.failure);
}
