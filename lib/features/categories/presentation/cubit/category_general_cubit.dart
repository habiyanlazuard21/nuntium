import 'package:bloc/bloc.dart';

import '../../../../lib.dart';

class CategoryGeneralCubit extends Cubit<CategoryGeneralState> {
  final GeneralRepository _generalRepository;

  CategoryGeneralCubit(this._generalRepository)
      : super(CategoryGeneralInitial());

  Future<void> getGeneralCategory({
    bool isLoading = true,
    required SelectedCategoryParams params,
  }) async {
    emit(CategoryGeneralLoading(isLoading));

    final response = await _generalRepository.getGeneralCategory(params);

    emit(
      response.fold(
        (failure) => CategoryGeneralError(failure),
        (list) => CategoryGeneralLoaded(generalList: list.data),
      ),
    );
  }
}
