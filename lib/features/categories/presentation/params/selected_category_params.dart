class SelectedCategoryParams {
  final String title;
  final String subtitle;
  String search;
  int selectedIndex;
  String category;

  SelectedCategoryParams({
    this.title = '',
    this.subtitle = '',
    this.search = '',
    this.selectedIndex = 0,
    this.category = '',
  });
}
