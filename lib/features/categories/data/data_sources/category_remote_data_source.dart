import 'package:dio/dio.dart';

import '../../../../lib.dart';

class CategoryRemoteDataSource extends BaseDataSource {
  final Dio _dio;

  CategoryRemoteDataSource(this._dio);

  Future<BaseApiResponseModel<List<ArticleModel>>> fetchCategoryGeneral(
      SelectedCategoryParams params) async {
    return dioCatchOrThrow(() async {
      final response =
          await _dio.get(UrlConstant.categoryGeneral, queryParameters: {
        'q': params.search,
        'country': 'id',
        'category': params.category,
        'apiKey': UrlConstant.apiKey,
      });

      return BaseApiResponseModel.fromJson(
        response.data as Map<String, dynamic>,
        generateData: (data) => List<ArticleModel>.from(
          (data as List).map(
            (e) => ArticleModel.fromJson(e as Map<String, Object?>),
          ),
        ),
      );
    });
  }
}
