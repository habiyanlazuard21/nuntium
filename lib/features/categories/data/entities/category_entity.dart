class CategoryEntity {
  final String category;
  final String subtitle;

  CategoryEntity(this.category, this.subtitle);

  static List<CategoryEntity> getCategory = [
    CategoryEntity("General", "Thousands of articles in each category"),
    CategoryEntity("Business", "Thousands of articles in each category"),
    CategoryEntity("Entertainment", "Thousands of articles in each category"),
    CategoryEntity("Health", "Thousands of articles in each category"),
    CategoryEntity("Science", "Thousands of articles in each category"),
    CategoryEntity("Sports", "Thousands of articles in each category"),
    CategoryEntity("Technology", "Thousands of articles in each category")
  ];
}
