class CategoryFilter {
  final String category;

  CategoryFilter(this.category);

  static List<CategoryFilter> getFilter = [
    CategoryFilter('general'),
    CategoryFilter('business'),
    CategoryFilter('entertainment'),
    CategoryFilter('health'),
    CategoryFilter('science'),
    CategoryFilter('sports'),
    CategoryFilter('technology'),
  ];
}
