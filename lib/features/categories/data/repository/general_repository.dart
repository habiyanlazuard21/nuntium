import 'package:either_dart/either.dart';

import '../../../../lib.dart';

class GeneralRepository extends BaseRepository {
  final CategoryRemoteDataSource _remoteDataSource;

  GeneralRepository(this._remoteDataSource);

  Future<Either<Failure, BaseApiResponseEntity<List<ArticleEntity>>>>
      getGeneralCategory(SelectedCategoryParams params) {
    return catchOrThrow(() async {
      final response = await _remoteDataSource.fetchCategoryGeneral(params);

      return BaseApiResponseEntity.fromBaseApiResponseModel(
        response,
        data: response.data?.map((e) => e.toArticleEntity()).toList(),
      );
    });
  }
}
