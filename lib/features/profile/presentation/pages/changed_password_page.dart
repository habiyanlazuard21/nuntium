import 'package:flutter/material.dart';
import 'package:nuntium_app/lib.dart';

class ChangedPasswordPage extends StatefulWidget {
  const ChangedPasswordPage({super.key});

  @override
  State<ChangedPasswordPage> createState() => _ChangedPasswordPageState();
}

class _ChangedPasswordPageState extends State<ChangedPasswordPage> {
  final List<TextFieldEntity> _changedPassword = TextFieldEntity.changePassword;
  final _formChangedPasswordKey = GlobalKey<FormState>();
  String _matchedPassword = '';

  @override
  void initState() {
    for (var dt in _changedPassword) {
      dt.textController.text = '';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: true,
      body: Form(
        key: _formChangedPasswordKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomAppBarProfile(
              centerTitle: true,
              title: "Change Password",
              elevation: 0,
            ),
            const Gap(height: AppGap.extraLarge),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
              child: CustomTextFormField(
                widgetPrefix: const Icon(
                  Icons.lock_outline,
                  weight: 1.5,
                ),
                textFieldEntity: _changedPassword[0],
                maxLines: 1,
              ),
            ),
            const Gap(height: AppGap.medium),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
              child: CustomTextFormField(
                widgetPrefix: const Icon(
                  Icons.lock_outline,
                  weight: 1.5,
                ),
                textFieldEntity: _changedPassword[1],
                maxLines: 1,
              ),
            ),
            const Gap(height: AppGap.medium),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
              child: CustomTextFormField(
                widgetPrefix: const Icon(
                  Icons.lock_outline,
                  weight: 1.5,
                ),
                textFieldEntity: _changedPassword[2],
                maxLines: 1,
                validator: (value) {
                  _matchedPassword = _changedPassword[1].textController.text;
                  final isMatched = value?.compareTo(_matchedPassword);

                  if (value == null || value.isEmpty) {
                    return 'Please enter your New password';
                  } else if (value.length < 8) {
                    return 'Password must be at least 8 characters';
                  } else if (isMatched != 0) {
                    return "Passwords do not match";
                  }

                  return null;
                },
              ),
            ),
            const Gap(height: AppGap.medium),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
              child: ButtonPrimary(
                "Changed Password",
                fontWeight: AppFontWeight.regular,
                width: double.infinity,
                fontSize: AppFontSize.medium,
                borderRadius: AppBorderRadius.normal,
                height: 56,
                onPressed: () {
                  if (_formChangedPasswordKey.currentState!.validate()) {
                    FocusScope.of(context).unfocus();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
