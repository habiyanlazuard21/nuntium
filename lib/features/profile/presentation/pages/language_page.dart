import 'package:flutter/material.dart';

import '../../../../lib.dart';

class LanguagePage extends StatefulWidget {
  const LanguagePage({super.key});

  @override
  State<LanguagePage> createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  final List<LanguagePageParams> _getList = LanguagePageParams.getLanguageList;
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomAppBarProfile(
            centerTitle: true,
            title: "Language",
            elevation: 0,
          ),
          const Gap(height: AppGap.extraLarge),
          Expanded(
            child: ListView.builder(
              itemCount: _getList.length,
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(
                horizontal: AppGap.large,
              ),
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: AppGap.medium),
                  child: CustomButtonCeklis(
                    _getList[index].language,
                    height: 56,
                    borderRadius: AppBorderRadius.normal,
                    paddingHorizontal: AppGap.large,
                    fontSize: AppFontSize.medium,
                    labelColor: _selectedIndex == index
                        ? AppColors.white
                        : AppColors.greyDarker,
                    fontWeight: AppFontWeight.regular,
                    buttonColor: _selectedIndex == index
                        ? AppColors.purplePrimary
                        : AppColors.greyLighter,
                    isShow: _selectedIndex == index ? true : false,
                    onPressed: () {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
