import 'package:flutter/material.dart';
import 'package:nuntium_app/lib.dart';

class PrivacyPage extends StatelessWidget {
  const PrivacyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomAppBarProfile(
            title: "Privacy",
            centerTitle: true,
            elevation: 0,
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              top: AppGap.large,
            ),
            child: Text(
              MessageConstant.privacy,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.greyPrimary,
                fontSize: AppFontSize.medium,
              ),
              textAlign: TextAlign.justify,
            ),
          ),
        ],
      ),
    );
  }
}
