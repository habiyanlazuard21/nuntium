class LanguagePageParams {
  final String language;

  const LanguagePageParams(this.language);

  static List<LanguagePageParams> getLanguageList = const [
    LanguagePageParams("English"),
    LanguagePageParams("Turkish"),
    LanguagePageParams("German"),
    LanguagePageParams("Spanish"),
  ];
}
