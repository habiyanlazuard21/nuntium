import 'package:flutter/material.dart';

import '../../../../lib.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: true,
      body: ColumnPadding(
        padding: const EdgeInsets.only(
          left: AppGap.large,
          right: AppGap.large,
          top: AppGap.big - AppGap.tiny,
        ),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Profile",
            style: AppTextStyle.regular.copyWith(
              color: AppColors.blackPrimary,
              fontSize: AppFontSize.big,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          ListTile(
            leading: const CustomImageWrapper(
              image: AppImageAssets.profile,
              width: 72,
              height: 72,
              borderRadius: AppBorderRadius.medium,
              fit: BoxFit.contain,
              isNetworkImage: false,
            ),
            title: Text(
              "Eren Turkmen",
              style: AppTextStyle.regular.copyWith(
                color: AppColors.blackPrimary,
                fontSize: AppFontSize.medium,
              ),
            ),
            subtitle: Text(
              "ertuken@gmail.com",
              style: AppTextStyle.regular.copyWith(
                color: AppColors.greyPrimary,
                fontSize: AppFontSize.normal,
              ),
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          CustomButtonNotification(
            "Notifications",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () {},
          ),
          const Gap(height: AppGap.medium),
          NewCustomButtonWithArrow(
            "Language",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () => Navigator.pushNamed(context, PagePath.language),
          ),
          const Gap(height: AppGap.medium),
          NewCustomButtonWithArrow(
            "Change Password",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () =>
                Navigator.pushNamed(context, PagePath.changedPassword),
          ),
          const Gap(height: AppGap.medium),
          NewCustomButtonWithArrow(
            "Privacy",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () => Navigator.pushNamed(context, PagePath.privacy),
          ),
          const Gap(height: AppGap.medium),
          NewCustomButtonWithArrow(
            "Terms & Conditions",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () => Navigator.pushNamed(
              context,
              PagePath.termAndConditions,
            ),
          ),
          const Gap(height: AppGap.medium),
          CustomButtonSignOut(
            "Sign Out",
            height: 56,
            buttonColor: AppColors.greyLighter,
            labelColor: AppColors.greyDarker,
            borderRadius: AppBorderRadius.normal,
            paddingHorizontal: AppGap.large,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            onPressed: () {
              context.confirmationDialog(
                message: "Are you sure, want to Logout?",
                onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context,
                  PagePath.signIn,
                  (route) => false,
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
