import 'package:nuntium_app/features/common/common.dart';

class ImageListEntity {
  final String image;

  ImageListEntity(this.image);

  static List<ImageListEntity> getListImage = [
    ImageListEntity(AppImageAssets.onBoardingImage),
    ImageListEntity(AppImageAssets.onBoardingImage),
    ImageListEntity(AppImageAssets.onBoardingImage),
  ];
}
