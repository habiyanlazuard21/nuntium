import 'package:flutter/material.dart';
import 'package:nuntium_app/features/common/common.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(
              top: ResponsiveUtils(context).getMediaQueryPaddingTop() +
                  AppGap.giant,
            ),
            child: Column(
              children: [
                const CustomImageWrapper(
                  image: AppImageAssets.shakingHand,
                  width: double.infinity,
                  height: 272,
                  isNetworkImage: false,
                ),
                const Spacer(),
                Text(
                  "Nuntium",
                  style: AppTextStyle.semiBold.copyWith(
                    color: AppColors.blackPrimary,
                    fontSize: AppFontSize.big,
                  ),
                ),
                const Gap(height: AppGap.extraLarge),
                Text(
                  'All News in one place, be the \nfirst to know last news',
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.greyPrimary,
                    fontSize: AppFontSize.medium,
                  ),
                  textAlign: TextAlign.center,
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.only(
                    left: AppGap.large,
                    right: AppGap.large,
                    bottom:
                        ResponsiveUtils(context).getMediaQueryPaddingBottom() +
                            50,
                  ),
                  child: ButtonPrimary(
                    "Get Started",
                    width: double.infinity,
                    fontSize: AppFontSize.medium,
                    height: 56,
                    buttonColor: AppColors.purplePrime,
                    borderRadius: AppBorderRadius.normal,
                    fontWeight: AppFontWeight.regular,
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        PagePath.signIn,
                        (route) => false,
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
