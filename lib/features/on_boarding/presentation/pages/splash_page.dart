import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../lib.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    Future.delayed(const Duration(milliseconds: 3000)).then((value) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        PagePath.onBoarding,
        (route) => false,
      );
    });

    return Scaffold(
      backgroundColor: AppColors.purplePrime,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            const CustomImageWrapper(
              image: AppIcon.logo,
              width: 48,
              height: 48,
              isNetworkImage: false,
            ),
            const Gap(height: AppGap.medium),
            Text(
              "Nuntium",
              style: AppTextStyle.medium.copyWith(
                color: AppColors.white,
                fontSize: AppFontSize.extraBig,
              ),
            )
          ],
        ),
      ),
    );
  }
}
