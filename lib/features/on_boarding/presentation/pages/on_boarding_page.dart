import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../lib.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  int indexActive = 0;

  final CarouselController carouselController = CarouselController();
  final List<ImageListEntity> getListImage = ImageListEntity.getListImage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      resizeToAvoidBottomInset: true,
      body: ColumnPadding(
        padding: EdgeInsets.only(
          top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 76,
        ),
        children: [
          CarouselSlider.builder(
            itemCount: getListImage.length,
            itemBuilder: (context, index, realIndex) {
              return CustomImageWrapper(
                image: getListImage[index].image,
                width: double.infinity,
                isNetworkImage: false,
              );
            },
            options: CarouselOptions(
              height: 336,
              aspectRatio: 16 / 9,
              viewportFraction: 0.8,
              initialPage: indexActive,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 3),
              autoPlayAnimationDuration: const Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              enlargeFactor: 0.3,
              onPageChanged: (index, reason) {
                setState(() {
                  indexActive = index;
                });
              },
            ),
          ),
          const Spacer(),
          AnimatedSmoothIndicator(
            activeIndex: indexActive,
            count: getListImage.length,
            effect: const WormEffect(
              spacing: AppGap.small,
              dotWidth: AppGap.normal,
              dotHeight: AppGap.normal,
              dotColor: AppColors.greyLighter,
              activeDotColor: AppColors.purplePrime,
            ),
          ),
          const Spacer(),
          Text(
            "First to know",
            style: AppTextStyle.medium.copyWith(
              color: AppColors.blackPrimary,
              fontSize: AppFontSize.big,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          Text(
            'All News in one place, be the \nfirst to know last news',
            style: AppTextStyle.regular.copyWith(
              color: AppColors.greyPrimary,
              fontSize: AppFontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
          const Spacer(),
          Padding(
            padding: EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              bottom:
                  ResponsiveUtils(context).getMediaQueryPaddingBottom() + 50,
            ),
            child: ButtonPrimary(
              "Next",
              width: double.infinity,
              fontSize: AppFontSize.medium,
              height: 56,
              buttonColor: AppColors.purplePrime,
              borderRadius: AppBorderRadius.normal,
              fontWeight: AppFontWeight.regular,
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  PagePath.welcome,
                  (route) => false,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
