import 'package:flutter/material.dart';

import '../../../../lib.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final String _desc =
      "We need your email adress so we can send you the password reset code.";

  final List<TextFieldEntity> _forgotPassword =
      TextFieldEntity.authForgotPassword;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // WidgetsBinding.instance.addPostFrameCallback((_) {

    // });

    _clearTextfield();

    super.initState();
  }

  void _clearTextfield() {
    for (var dt in _forgotPassword) {
      dt.textController.text = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ColumnPadding(
        crossAxisAlignment: CrossAxisAlignment.start,
        padding: EdgeInsets.only(
          left: AppGap.large,
          right: AppGap.large,
          top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
        ),
        children: [
          Text(
            "Forgot Password 🤔",
            style: AppTextStyle.regular.copyWith(
              color: AppColors.blackPrimary,
              fontSize: AppFontSize.big,
            ),
          ),
          const Gap(height: AppGap.small),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.greyPrimary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          Form(
            key: _formKey,
            child: CustomTextFormField(
              widgetPrefix: const Icon(
                Icons.email_outlined,
                weight: 1,
              ),
              textFieldEntity: _forgotPassword[0],
              backgroundDisable: TextFieldColors.backgroundDisable,
              maxLines: 1,
            ),
          ),
          const Gap(height: AppGap.medium),
          ButtonPrimary(
            "Next",
            width: double.infinity,
            height: 56,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            borderRadius: AppBorderRadius.normal,
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                FocusScope.of(context).unfocus();

                Navigator.pushNamed(context, PagePath.verification);
                _clearTextfield();
              }
            },
          ),
          const Spacer(),
          RowPadding(
            padding: EdgeInsets.only(
              bottom:
                  ResponsiveUtils(context).getMediaQueryPaddingBottom() + 42,
            ),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Remember the password?",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.blackLighter,
                  fontSize: AppFontSize.medium,
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, PagePath.signIn);
                },
                child: Text(
                  "Try again",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.blackPrimary,
                    fontSize: AppFontSize.medium,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
