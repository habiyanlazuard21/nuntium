import 'package:flutter/material.dart';
import 'package:nuntium_app/lib.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final String _desc =
      'I am happy to see you again. You can continue where you left off by logging in';

  final List<TextFieldEntity> _singInEntity = TextFieldEntity.authLogin;
  final _formVerificationKey = GlobalKey<FormState>();

  @override
  void initState() {
    for (var dt in _singInEntity) {
      dt.textController.text = '';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      resizeToAvoidBottomInset: false,
      extendBody: true,
      body: Form(
        key: _formVerificationKey,
        child: ColumnPadding(
          crossAxisAlignment: CrossAxisAlignment.start,
          padding: EdgeInsets.only(
            left: AppGap.large,
            right: AppGap.large,
            top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
          ),
          children: [
            Text(
              "Welcome Back 👋",
              style: AppTextStyle.regular.copyWith(
                color: AppColors.blackPrimary,
                fontSize: AppFontSize.big,
              ),
            ),
            const Gap(height: AppGap.small),
            Text(
              _desc,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.greyPrimary,
                fontSize: AppFontSize.medium,
              ),
            ),
            const Gap(height: AppGap.big + AppGap.tiny),
            CustomTextFormField(
              widgetPrefix: const Icon(
                Icons.email_outlined,
                weight: 1.5,
              ),
              textFieldEntity: _singInEntity[0],
              maxLines: 1,
            ),
            const Gap(height: AppGap.medium),
            CustomTextFormField(
              widgetPrefix: const Icon(
                Icons.lock,
                weight: 1.5,
              ),
              textFieldEntity: _singInEntity[1],
              backgroundDisable: TextFieldColors.backgroundDisable,
              maxLines: 1,
            ),
            const Gap(height: AppGap.medium),
            Container(
              alignment: Alignment.topRight,
              padding: const EdgeInsets.only(
                right: AppGap.small,
              ),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, PagePath.forgotPassword);
                },
                child: Text(
                  "Forgot Password?",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.greyPrimary,
                    fontSize: AppFontSize.medium,
                  ),
                ),
              ),
            ),
            const Gap(height: AppGap.extraLarge),
            ButtonPrimary(
              "Sign in",
              width: double.infinity,
              height: 56,
              fontSize: AppFontSize.medium,
              fontWeight: AppFontWeight.regular,
              borderRadius: AppBorderRadius.normal,
              onPressed: () {
                if (_formVerificationKey.currentState!.validate()) {
                  FocusScope.of(context).unfocus();
                  Navigator.pushNamedAndRemoveUntil(
                      context, PagePath.bottomNavBar, (route) => false);
                }
              },
            ),
            const Spacer(),
            Center(
              child: Text(
                "or",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.greyPrimary,
                  fontSize: AppFontSize.medium,
                ),
              ),
            ),
            const Spacer(),
            CustomButtonWithIconImage(
              icon: AppIcon.google,
              label: "Sign In with Google",
              height: 56,
              fontSize: AppFontSize.medium,
              buttonColor: Colors.transparent,
              borderColor: AppColors.greyLighter,
              fontWeight: AppFontWeight.regular,
              borderWidth: 1,
              borderRadius: AppBorderRadius.normal,
              labelColor: AppColors.greyDarker,
              onPressed: () {},
            ),
            const Gap(height: AppGap.medium),
            CustomButtonWithIconImage(
              icon: AppIcon.facebook,
              label: "Sign In with Facebook",
              height: 56,
              fontSize: AppFontSize.medium,
              buttonColor: Colors.transparent,
              borderColor: AppColors.greyLighter,
              fontWeight: AppFontWeight.regular,
              borderWidth: 1,
              borderRadius: AppBorderRadius.normal,
              labelColor: AppColors.greyDarker,
              onPressed: () {},
            ),
            const Spacer(),
            RowPadding(
              padding: EdgeInsets.only(
                bottom:
                    ResponsiveUtils(context).getMediaQueryPaddingBottom() + 42,
              ),
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Don't have an Account?",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.blackLighter,
                    fontSize: AppFontSize.medium,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpScreen(),
                      ),
                    );
                  },
                  child: Text(
                    "Sign Up",
                    style: AppTextStyle.regular.copyWith(
                      color: AppColors.blackPrimary,
                      fontSize: AppFontSize.medium,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
