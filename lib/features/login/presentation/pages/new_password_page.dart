import 'package:flutter/material.dart';

import '../../../../lib.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});

  @override
  State<NewPassword> createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  final String _desc =
      "You can create a new password, please dont forget it too.";
  String _matchedPassword = '';
  final List<TextFieldEntity> _passwordTextFieldEntity =
      TextFieldEntity.changePassword;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _clearTextField();

    super.initState();
  }

  void _clearTextField() {
    for (var dt in _passwordTextFieldEntity) {
      dt.textController.text = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: ColumnPadding(
          crossAxisAlignment: CrossAxisAlignment.start,
          padding: EdgeInsets.only(
            left: AppGap.large,
            right: AppGap.large,
            top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
          ),
          children: [
            Text(
              "Create New Password 🔒",
              style: AppTextStyle.regular.copyWith(
                color: AppColors.blackPrimary,
                fontSize: AppFontSize.big,
              ),
            ),
            const Gap(height: AppGap.small),
            Text(
              _desc,
              style: AppTextStyle.regular.copyWith(
                color: AppColors.greyPrimary,
                fontSize: AppFontSize.medium,
              ),
            ),
            const Gap(height: AppGap.big + AppGap.tiny),
            CustomTextFormField(
              widgetPrefix: const Icon(
                Icons.lock,
                weight: 1.5,
              ),
              textFieldEntity: _passwordTextFieldEntity[0],
              maxLines: 1,
            ),
            const Gap(height: AppGap.medium),
            CustomTextFormField(
              widgetPrefix: const Icon(
                Icons.lock,
                weight: 1.5,
              ),
              textFieldEntity: _passwordTextFieldEntity[1],
              maxLines: 1,
              validator: (value) {
                _matchedPassword =
                    _passwordTextFieldEntity[0].textController.text;
                final _isMatched = value?.compareTo(_matchedPassword);

                if (value == null || value.isEmpty) {
                  return 'Please enter your password';
                } else if (value.length < 8) {
                  return 'Password must be at least 8 characters';
                } else if (_isMatched != 0) {
                  return "Passwords do not match";
                }

                return null;
              },
            ),
            const Gap(height: AppGap.medium),
            ButtonPrimary(
              "Confirm",
              width: double.infinity,
              height: 56,
              fontSize: AppFontSize.medium,
              fontWeight: AppFontWeight.regular,
              borderRadius: AppBorderRadius.normal,
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  FocusScope.of(context).unfocus();
                }
              },
            ),
            const Spacer(),
            RowPadding(
              padding: EdgeInsets.only(
                bottom:
                    ResponsiveUtils(context).getMediaQueryPaddingBottom() + 42,
              ),
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Didn’t receive an email?",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.blackLighter,
                    fontSize: AppFontSize.medium,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, PagePath.newPassword);
                  },
                  child: Text(
                    "Send again",
                    style: AppTextStyle.regular.copyWith(
                      color: AppColors.blackPrimary,
                      fontSize: AppFontSize.medium,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
