import 'package:flutter/material.dart';

import '../../../../lib.dart';

class VerificationPages extends StatefulWidget {
  const VerificationPages({super.key});

  @override
  State<VerificationPages> createState() => _VerificationPagesState();
}

class _VerificationPagesState extends State<VerificationPages> {
  final String _desc =
      "You need to enter 4-digit code we send to your email adress";

  final _formVerificationKey = GlobalKey<FormState>();
  final List<TextFieldEntity> _verificationOtpEntity =
      TextFieldEntity.verificationOTP;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _clearTextField();
    });

    super.initState();
  }

  void _clearTextField() {
    for (var dt in _verificationOtpEntity) {
      dt.textController.text = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      resizeToAvoidBottomInset: false,
      body: ColumnPadding(
        crossAxisAlignment: CrossAxisAlignment.start,
        padding: EdgeInsets.only(
          left: AppGap.large,
          right: AppGap.large,
          top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
        ),
        children: [
          Text(
            "Verification Code ✅",
            style: AppTextStyle.regular.copyWith(
              color: AppColors.blackPrimary,
              fontSize: AppFontSize.big,
            ),
          ),
          const Gap(height: AppGap.small),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.greyPrimary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          Form(
            key: _formVerificationKey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextFormFieldOtp(
                  textFieldEntity: _verificationOtpEntity[0],
                  onChanged: (value) {
                    if (value.length == 1) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[1].focusNode);
                    }
                  },
                ),
                CustomTextFormFieldOtp(
                  textFieldEntity: _verificationOtpEntity[1],
                  onChanged: (value) {
                    if (value.length == 1) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[2].focusNode);
                    }
                    if (value.isEmpty) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[0].focusNode);
                    }
                  },
                ),
                CustomTextFormFieldOtp(
                  textFieldEntity: _verificationOtpEntity[2],
                  onChanged: (value) {
                    if (value.length == 1) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[3].focusNode);
                    }
                    if (value.isEmpty) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[1].focusNode);
                    }
                  },
                ),
                CustomTextFormFieldOtp(
                  textFieldEntity: _verificationOtpEntity[3],
                  onChanged: (value) {
                    if (value.length == 1) {
                      FocusScope.of(context).unfocus();
                    }
                    if (value.isEmpty) {
                      FocusScope.of(context)
                          .requestFocus(_verificationOtpEntity[2].focusNode);
                    }
                  },
                ),
              ],
            ),
          ),
          const Gap(height: AppGap.medium),
          ButtonPrimary(
            "Confirm",
            width: double.infinity,
            height: 56,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            borderRadius: AppBorderRadius.normal,
            onPressed: () {
              if (_formVerificationKey.currentState!.validate()) {
                FocusScope.of(context).unfocus();
                Navigator.pushNamed(context, PagePath.newPassword).then(
                  (result) {
                    _clearTextField();
                  },
                );
              }
            },
          ),
          const Spacer(),
          RowPadding(
            padding: EdgeInsets.only(
              bottom:
                  ResponsiveUtils(context).getMediaQueryPaddingBottom() + 42,
            ),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Didn’t receive an email?",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.blackLighter,
                  fontSize: AppFontSize.medium,
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, PagePath.newPassword);
                },
                child: Text(
                  "Send again",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.blackPrimary,
                    fontSize: AppFontSize.medium,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
