import 'package:flutter/material.dart';

import '../../../../lib.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final String _desc =
      "Hello, I guess you are new around here. You can start using the application after sign up";

  final List<TextFieldEntity> _authRegister = TextFieldEntity.authRegister;
  String _matchedPassword = "";

  @override
  void initState() {
    for (var dt in _authRegister) {
      dt.textController.text = '';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      resizeToAvoidBottomInset: false,
      body: ColumnPadding(
        crossAxisAlignment: CrossAxisAlignment.start,
        padding: EdgeInsets.only(
          left: AppGap.large,
          right: AppGap.large,
          top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
        ),
        children: [
          Text(
            "Welcome to Nuntium 👋",
            style: AppTextStyle.regular.copyWith(
              color: AppColors.blackPrimary,
              fontSize: AppFontSize.big,
            ),
          ),
          const Gap(height: AppGap.small),
          Text(
            _desc,
            style: AppTextStyle.regular.copyWith(
              color: AppColors.greyPrimary,
              fontSize: AppFontSize.medium,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          CustomTextFormField(
            widgetPrefix: const Icon(
              Icons.person_2,
              weight: 1.5,
            ),
            textFieldEntity: _authRegister[0],
            backgroundDisable: TextFieldColors.backgroundDisable,
            maxLines: 1,
          ),
          const Gap(height: AppGap.small),
          CustomTextFormField(
            widgetPrefix: const Icon(
              Icons.email_outlined,
              weight: 1.5,
            ),
            textFieldEntity: _authRegister[1],
            backgroundDisable: TextFieldColors.backgroundDisable,
            maxLines: 1,
          ),
          const Gap(height: AppGap.small),
          CustomTextFormField(
            widgetPrefix: const Icon(
              Icons.lock,
              weight: 1.5,
            ),
            textFieldEntity: _authRegister[2],
            maxLines: 1,
          ),
          const Gap(height: AppGap.small),
          CustomTextFormField(
            widgetPrefix: const Icon(
              Icons.lock,
              weight: 1.5,
            ),
            textFieldEntity: _authRegister[3],
            backgroundDisable: TextFieldColors.backgroundDisable,
            maxLines: 1,
            validator: (value) {
              _matchedPassword = _authRegister[2].textController.text;
              final _isMatched = value?.compareTo(_matchedPassword);

              if (value == null || value.isEmpty) {
                return 'Please enter your password';
              } else if (value.length < 8) {
                return 'Password must be at least 8 characters';
              } else if (_isMatched != 0) {
                return "Passwords do not match";
              }

              return null;
            },
          ),
          const Gap(height: AppGap.small),
          ButtonPrimary(
            "Sign Up",
            width: double.infinity,
            height: 56,
            fontSize: AppFontSize.medium,
            fontWeight: AppFontWeight.regular,
            borderRadius: AppBorderRadius.normal,
            onPressed: () {},
          ),
          const Spacer(),
          RowPadding(
            padding: EdgeInsets.only(
              bottom:
                  ResponsiveUtils(context).getMediaQueryPaddingBottom() + 42,
            ),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Already have an account?",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.blackLighter,
                  fontSize: AppFontSize.medium,
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "Sign In",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.blackPrimary,
                    fontSize: AppFontSize.medium,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
