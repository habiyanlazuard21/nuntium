class VerificationOtpPageParams {
  final String email;
  final bool isForgotPassword;

  VerificationOtpPageParams({
    required this.email,
    this.isForgotPassword = false,
  });
}
