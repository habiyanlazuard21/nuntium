import '../../../../lib.dart';

// class NewsDataModel {
//   List<ArticleModel>? articles;

//   NewsDataModel({this.articles});

//   factory NewsDataModel.fromJson(Map<String, dynamic> json) {
//     return NewsDataModel(
//       articles: (json['articles'] as List<dynamic>?)
//           ?.map((e) => ArticleModel.fromJson(e as Map<String, dynamic>))
//           .toList(),
//     );
//   }

//   Map<String, dynamic> toJson() {
//     return {
//       "articles": articles?.map((e) => e.toJson()).toList(),
//     };
//   }

//   NewsDataEntity toNewsDataEntity() {
//     return NewsDataEntity(
//       articles: articles?.map((e) => e.toArticleEntity()).toList(),
//     );
//   }
// }

class ArticleModel {
  SourceArticleModel? source;
  String? author;
  String? title;
  String? description;
  String? url;
  String? urlToImage;
  DateTime? publishedAt;
  String? content;

  ArticleModel({
    this.source,
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content,
  });

  factory ArticleModel.fromJson(Map<String, dynamic> json) {
    return ArticleModel(
      source: json["source"] == null
          ? null
          : SourceArticleModel.fromJson(json["source"]),
      author: json["author"] as String?,
      title: json["title"] as String?,
      description: json["description"] as String?,
      url: json["url"] ?? '',
      urlToImage: json["urlToImage"] as String?,
      publishedAt: json["publishedAt"] == null
          ? null
          : DateTime.parse(json["publishedAt"]),
      content: json["content"] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "source": source?.toJson(),
      "author": author,
      "title": title,
      "description": description,
      "url": url,
      "urlToImage": urlToImage,
      "publishedAt": publishedAt?.toIso8601String(),
      "content": content,
    };
  }

  ArticleEntity toArticleEntity() {
    return ArticleEntity(
      source: source?.toEntity(),
      author: author,
      title: title,
      description: description,
      url: url,
      urlToImage: urlToImage,
      publishedAt: publishedAt,
      content: content,
    );
  }
}

class SourceArticleModel {
  String? id;
  String? name;

  SourceArticleModel({
    this.id,
    this.name,
  });

  factory SourceArticleModel.fromJson(Map<String, dynamic> json) {
    return SourceArticleModel(
      id: json["id"] ?? '',
      name: json["name"] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
    };
  }

  SourceArticleEntity toEntity() => SourceArticleEntity(
        id: id,
        name: name,
      );
}

// extension NewsDataModelExtension on NewsDataModel {
//   NewsDataEntity toNewsDataEntity() {
//     return NewsDataEntity(
//       articles: articles?.map((article) => article.toArticleEntity()).toList(),
//     );
//   }
// }
