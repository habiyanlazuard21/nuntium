// news_entity.dart

// class NewsDataEntity {
//   final List<ArticleEntity>? articles;

//   NewsDataEntity({required this.articles});
// }

class ArticleEntity {
  final SourceArticleEntity? source;
  final String? author;
  final String? title;
  final String? description;
  final String? url;
  final String? urlToImage;
  final DateTime? publishedAt;
  final String? content;

  ArticleEntity({
    this.source,
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content,
  });
}

class SourceArticleEntity {
  final String? id;
  final String? name;

  SourceArticleEntity({this.id, this.name});
}
