import 'package:dio/dio.dart';

import '../../../../lib.dart';

class NewsRemoteDataSource with BaseDataSource {
  final Dio _dio;

  NewsRemoteDataSource(this._dio);

  Future<BaseApiResponseModel<List<ArticleModel>>> fetchDataApi(
    HomeScreenParams params,
  ) async {
    return dioCatchOrThrow(() async {
      final response = await _dio.get(
        UrlConstant.getNews,
        queryParameters: {
          'q': params.search,
          'apiKey': UrlConstant.apiKey,
        },
      );

      return BaseApiResponseModel.fromJson(
        response.data as Map<String, dynamic>,
        generateData: (data) => List<ArticleModel>.from(
          (data as List).map(
            (e) => ArticleModel.fromJson(e as Map<String, Object?>),
          ),
        ),
      );
    });
  }
}
