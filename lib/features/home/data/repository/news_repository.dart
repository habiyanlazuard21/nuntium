import 'package:either_dart/either.dart';

import '../../../../lib.dart';

class NewsRepository with BaseRepository {
  final NewsRemoteDataSource _remoteDataSource;

  NewsRepository(this._remoteDataSource);

  Future<Either<Failure, BaseApiResponseEntity<List<ArticleEntity>>>>
      getNewsApi(
    HomeScreenParams params,
  ) {
    return catchOrThrow(() async {
      final response = await _remoteDataSource.fetchDataApi(params);

      return BaseApiResponseEntity.fromBaseApiResponseModel(
        response,
        data: response.data?.map((e) => e.toArticleEntity()).toList(),
      );
    });
  }
}
