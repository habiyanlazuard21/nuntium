import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuntium_app/injection.dart';

import '../../../../lib.dart';

class HomeScreen extends StatefulWidget {
  final HomeScreenParams _params;
  const HomeScreen({
    Key? key,
    required HomeScreenParams params,
  })  : _params = params,
        super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<ArticleEntity> listArticle = [];

  @override
  void initState() {
    getIt<GetArticleCubit>().getArticle(params: HomeScreenParams());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<GetArticleCubit>()..getArticle(params: widget._params),
      child: BlocConsumer<GetArticleCubit, GetArticleState>(
        listener: (context, state) {
          if (state is GetArticleError) {
            context.errorDialog(message: state.failure.message);
          }
        },
        builder: (context, state) {
          if (state is GetArticleLoaded) {
            listArticle.clear();

            listArticle.addAll(state.listArticleEntity ?? []);
          } else if (state is GetArticleError) {
            // listArticle.clear();
          }

          return _HomeScreenWrapper(
            listArticle: listArticle,
            params: widget._params,
            isLoading: state is GetArticleLoading && state.isLoading,
          );
        },
      ),
    );
  }
}

class _HomeScreenWrapper extends StatefulWidget {
  const _HomeScreenWrapper({
    Key? key,
    required List<ArticleEntity> listArticle,
    required bool isLoading,
    required HomeScreenParams params,
  })  : _listArticle = listArticle,
        _params = params,
        _isLoading = isLoading,
        super(key: key);

  final HomeScreenParams _params;
  final List<ArticleEntity> _listArticle;
  final bool _isLoading;

  @override
  State<_HomeScreenWrapper> createState() => _HomeScreenWrapperState();
}

class _HomeScreenWrapperState extends State<_HomeScreenWrapper> {
  final TextEditingController _searchArticleController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: AppGap.large,
              right: AppGap.large,
              top: ResponsiveUtils(context).getMediaQueryPaddingTop() + 28,
            ),
            child: const CustomAppBar(
              elevation: 0,
              title: "Browse",
              subtitle: "Discover things of this world",
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppGap.large),
            child: CustomTextFormFieldSearch(
              controller: _searchArticleController,
              onPressed: () {
                FocusScope.of(context).unfocus();

                setState(() {
                  widget._params.search = _searchArticleController.text.trim();
                });

                context
                    .read<GetArticleCubit>()
                    .getArticle(params: widget._params);
              },
              onSubmitted: (value) {
                FocusScope.of(context).unfocus();

                setState(() {
                  widget._params.search = value.trim();
                });
                context
                    .read<GetArticleCubit>()
                    .getArticle(params: widget._params);
              },
            ),
          ),
          const Gap(height: AppGap.extraLarge / 2),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                _searchArticleController.text = "";
                setState(() {
                  widget._params.search = _searchArticleController.text.trim();
                  widget._listArticle.clear();
                });

                context
                    .read<GetArticleCubit>()
                    .getArticle(params: widget._params);
              },
              child: CustomScrollViewWrapper(
                slivers: [
                  /* Loading Indicator */
                  SliverVisibility(
                    visible: widget._isLoading,
                    sliver: const SliverFillRemaining(
                      hasScrollBody: false,
                      child: LoadingIndicator(),
                    ),
                  ),

                  /* List Article */
                  SliverVisibility(
                    visible:
                        widget._listArticle.isNotEmpty && !widget._isLoading,
                    sliver: SliverColumnPadding(
                      mainAxisSize: MainAxisSize.min,
                      padding: EdgeInsets.only(
                        top:
                            ResponsiveUtils(context).getMediaQueryPaddingTop() +
                                AppGap.normal,
                        left: AppGap.large,
                        right: AppGap.large,
                      ),
                      children: [
                        ...List.generate(
                            widget._listArticle.length > 15
                                ? 15
                                : widget._listArticle.length, (index) {
                          return ItemArticle(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewPage(
                                    params: WebViewPageParams(
                                      url: widget._listArticle[index].url ?? '',
                                    ),
                                  ),
                                ),
                              );
                            },
                            articleItemEntity: widget._listArticle[index],
                          );
                        }),
                      ],
                    ),
                  ),

                  /* Handle Empty data */
                  SliverVisibility(
                    visible: widget._listArticle.isEmpty && !widget._isLoading,
                    sliver: const SliverFillRemaining(
                      hasScrollBody: false,
                      child: EmptyData(),
                    ),
                  ),

                  /* bottom padding */
                  SliverVisibility(
                    visible:
                        widget._listArticle.isNotEmpty && !widget._isLoading,
                    sliver: SliverGap(
                      height:
                          ResponsiveUtils(context).getResponsiveBottomPadding(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ItemArticle extends StatelessWidget {
  const ItemArticle({
    Key? key,
    required ArticleEntity articleItemEntity,
    required Function() onTap,
  })  : _articleItemEntity = articleItemEntity,
        _onTap = onTap,
        super(key: key);

  final ArticleEntity _articleItemEntity;
  final Function() _onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.only(bottom: AppGap.medium),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(AppGap.normal),
        ),
        height: 96,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Visibility(
              visible: _articleItemEntity.urlToImage?.isNotEmpty ?? false,
              child: CustomImageWrapper(
                image: _articleItemEntity.urlToImage.toString(),
                width: 96,
                borderRadius: AppBorderRadius.normal,
                isNetworkImage: true,
              ),
            ),
            Visibility(
              visible: _articleItemEntity.urlToImage?.isEmpty ?? true,
              child: const CustomImageWrapper(
                image: AppImageAssets.drawing,
                width: 96,
                borderRadius: AppBorderRadius.normal,
                isNetworkImage: false,
              ),
            ),
            Expanded(
              child: ColumnPadding(
                padding: const EdgeInsets.only(
                  left: AppGap.medium,
                  right: AppGap.normal,
                ),
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          _articleItemEntity.author ?? '',
                          style: AppTextStyle.regular.copyWith(
                            color: AppColors.greyPrimary,
                            fontSize: AppFontSize.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: const Icon(
                          Icons.bookmark_border,
                          size: AppIconSize.medium,
                          color: AppColors.purpleDarker,
                        ),
                      )
                    ],
                  ),
                  const Gap(height: AppGap.small),
                  Text(
                    _articleItemEntity.title ?? '',
                    style: AppTextStyle.regular.copyWith(
                      fontSize: AppFontSize.medium,
                      color: AppColors.blackPrimary,
                      overflow: TextOverflow.ellipsis,
                    ),
                    maxLines: 2,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
