part of 'get_article_cubit.dart';

abstract class GetArticleState extends Equatable {
  const GetArticleState();

  @override
  List<Object> get props => [];
}

class GetArticleInitial extends GetArticleState {}

class GetArticleLoading extends GetArticleState {
  final bool isLoading;

  const GetArticleLoading({required this.isLoading});
}

class GetArticleLoaded extends GetArticleState {
  final List<ArticleEntity>? listArticleEntity;

  const GetArticleLoaded({
    this.listArticleEntity,
  });
}

class GetArticleError extends GetArticleState {
  final Failure failure;

  const GetArticleError(this.failure);
}
