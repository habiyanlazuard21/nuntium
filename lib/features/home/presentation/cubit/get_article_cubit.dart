import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../lib.dart';

part 'get_article_state.dart';

class GetArticleCubit extends Cubit<GetArticleState> {
  final NewsRepository _newsRepository;

  GetArticleCubit(this._newsRepository) : super(GetArticleInitial());

  Future<void> getArticle({
    required HomeScreenParams params,
    bool isLoading = true,
  }) async {
    emit(
      GetArticleLoading(isLoading: isLoading),
    );

    final response = await _newsRepository.getNewsApi(params);

    emit(
      response.fold(
        (failure) => GetArticleError(failure),
        (list) => GetArticleLoaded(listArticleEntity: list.data),
      ),
    );
  }
}
