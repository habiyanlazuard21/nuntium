import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../lib.dart';

class BottomNavigation extends StatefulWidget {
  final int _selectedIndex;
  const BottomNavigation({
    Key? key,
    int selectedIndex = 0,
  })  : _selectedIndex = selectedIndex,
        super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int _activeIndex = 0;
  final List<BottomNavigationEntity> _bottomNavList =
      BottomNavigationEntity.bottomNavList;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      // systemNavigationBarColor: AppColors.blackPrimary,
      statusBarBrightness: Brightness.light,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    return ScaffoldConstraint(
      keyScaffold: _key,
      onWillPop: () {},
      isBottomNavBar: true,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: _bottomNavigation(ctx),
      child: Column(
        children: [
          Expanded(
            child: IndexedStack(
              index: _activeIndex,
              children: [
                HomeScreen(params: HomeScreenParams()),
                CategoriScreen(params: CategoryScreenParams(selectedIndex: 0)),
                const Center(
                  child: Text("data"),
                ),
                const ProfileScreen(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _bottomNavigation(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.greyLighter, width: 1),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(AppBorderRadius.normal),
            topRight: Radius.circular(AppBorderRadius.normal),
          ),
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, -1),
              blurRadius: 0,
              spreadRadius: 0,
              color: AppColors.white,
            )
          ]),
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.transparent,
        elevation: 0,
        items: <BottomNavigationBarItem>[
          ...List.generate(
            _bottomNavList.length,
            (index) => BottomNavigationBarItem(
              icon: _bottomNavList[index].iconUnActive,
              activeIcon: _bottomNavList[index].iconActive,
              label: _bottomNavList[index].label,
            ),
          )
        ],
        currentIndex: _activeIndex,
        // selectedItemColor: AppColors.purplePrime,
        // unselectedItemColor: AppColors.white,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        onTap: (index) => _onItemTapped(index, context),
      ),
    );
  }

  Future<void> _onItemTapped(int index, BuildContext ctx) async {
    FocusScope.of(context).unfocus();
    FocusScope.of(context).requestFocus(FocusNode());

    setState(() {
      _activeIndex = index;
    });
  }
}
