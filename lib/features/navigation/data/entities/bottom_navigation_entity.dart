import 'package:flutter/material.dart';

import '../../../../lib.dart';

class BottomNavigationEntity {
  final Widget iconUnActive;
  final Widget iconActive;
  final String label;

  BottomNavigationEntity({
    required this.iconActive,
    required this.iconUnActive,
    required this.label,
  });

  static List<BottomNavigationEntity> bottomNavList = [
    BottomNavigationEntity(
      iconUnActive: const CustomSvgWrapper(
        svgPath: AppIcon.homeGrey,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      iconActive: const CustomSvgWrapper(
        svgPath: AppIcon.homePurple,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      label: 'Home',
    ),
    BottomNavigationEntity(
      iconUnActive: const CustomSvgWrapper(
        svgPath: AppIcon.appGrey,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      iconActive: const CustomSvgWrapper(
        svgPath: AppIcon.appPurple,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      label: 'Apps',
    ),
    BottomNavigationEntity(
      iconUnActive: const CustomSvgWrapper(
        svgPath: AppIcon.bookmarkGrey,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      iconActive: const CustomSvgWrapper(
        svgPath: AppIcon.bookmarkPurple,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      label: 'Bookmark',
    ),
    BottomNavigationEntity(
      iconUnActive: const CustomSvgWrapper(
        svgPath: AppIcon.usergrey,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      iconActive: const CustomSvgWrapper(
        svgPath: AppIcon.userPurple,
        width: 18,
        height: 20,
        isNetwork: false,
      ),
      label: 'User',
    ),
  ];
}
