import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:nuntium_app/core/constant/constant.dart';
import 'package:nuntium_app/core/data/remote/dio_client.dart';
import 'package:nuntium_app/features/categories/categories.dart';
import 'package:nuntium_app/features/home/home.dart';

final getIt = GetIt.instance;

Future<void> init() async {
  await _core();

  _article();
  _category();
}

Future<void> _core() async {
  getIt.registerLazySingleton<Dio>(
    () => DioClient.dioInit(baseUrl: UrlConstant.baseUrl),
  );
}

void _article() {
  getIt.registerLazySingleton(() => NewsRemoteDataSource(getIt()));
  getIt.registerLazySingleton(() => NewsRepository(getIt()));
  getIt.registerFactory(() => GetArticleCubit(getIt()));
}

void _category() {
  getIt.registerLazySingleton(() => CategoryRemoteDataSource(getIt()));
  getIt.registerLazySingleton(() => GeneralRepository(getIt()));
  getIt.registerFactory(() => CategoryGeneralCubit(getIt()));
}
