import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuntium_app/lib.dart';

import 'injection.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final PageRouter router = PageRouter();

    return MultiBlocProvider(
      providers: [
        BlocProvider.value(
          value: di.getIt<CategoryGeneralCubit>(),
        ),
        BlocProvider.value(
          value: di.getIt<GetArticleCubit>(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: Nuntium().of(context),
        onGenerateRoute: router.getRoute,
      ),
    );
  }
}
