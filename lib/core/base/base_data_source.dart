import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../lib.dart';

abstract class BaseDataSource {
  Future<T> dioCatchOrThrow<T>(Future<T> Function() body) async {
    try {
      return await body();
    } on SocketException {
      throw const RemoteException(
        message: MessageConstant.noInternetConnection,
      );
    } on DioException catch (e) {
      bool checkConnection = kIsWeb ? true : await _isHasInternetConnection();
      if (!checkConnection ||
          (e.message != null && e.message!.contains('SocketException'))) {
        throw const RemoteException(
          message: MessageConstant.noInternetConnection,
        );
      } else {
        final response = e.response;

        if (response != null) {
          final errorModel = BaseApiResponseModel.fromJson(
            response.data as Map<String, Object?>,
          );
          throw RemoteException(
            message: errorModel.message,
            error: BaseApiResponseEntity.fromBaseApiResponseModel(errorModel),
          );
        }

        throw RemoteException(message: e.message);
      }
    } catch (e) {
      throw e is RemoteException
          ? e
          : const RemoteException(message: MessageConstant.defaultErrorMessage);
    }
  }

  Future<bool> _isHasInternetConnection() async {
    return await InternetConnectionChecker().hasConnection;
  }
}
