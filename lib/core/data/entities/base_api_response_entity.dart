// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import '../../../lib.dart';

class BaseApiResponseEntity<T> extends Equatable {
  final String status;
  final int? totalResults;
  final T? data;

  const BaseApiResponseEntity({
    required this.status,
    required this.totalResults,
    this.data,
  });

  factory BaseApiResponseEntity.fromBaseApiResponseModel(
    BaseApiResponseModel response, {
    T? data,
  }) =>
      BaseApiResponseEntity(
        status: response.status,
        totalResults: response.totalResults,
        data: data,
      );

  @override
  List<Object?> get props {
    return [
      status,
      totalResults,
      data,
    ];
  }

  BaseApiResponseEntity<T> copyWith({
    String? status,
    int? totalResults,
    T? data,
  }) {
    return BaseApiResponseEntity<T>(
      status: status ?? this.status,
      totalResults: totalResults ?? this.totalResults,
      data: data ?? this.data,
    );
  }

  @override
  bool get stringify => true;
}
