import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class DioClient {
  static Dio dioInit({
    required String baseUrl,
  }) {
    return Dio()
      ..options = BaseOptions(
        baseUrl: baseUrl,
        connectTimeout: const Duration(seconds: 90),
        sendTimeout: const Duration(seconds: 90),
        receiveTimeout: const Duration(seconds: 90),
      )
      ..interceptors.addAll([
        // Show log network request in debug console [only in debug mode]
        if (kDebugMode) ...[
          LogInterceptor(
            requestBody: true,
            responseBody: true,
            logPrint: (message) => log(message.toString()),
          ),
        ],
        CustomInterceptor(),
      ]);
  }
}

class CustomInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // final user = getIt<UserCubit>().state.loginEntity;

    // if (user != null) {
    //   options.headers.addAll({
    //     'X-TOKEN': '${user.token}',
    //   });
    // }

    super.onRequest(options, handler);
  }

  @override
  Future<void> onError(
      DioException err, ErrorInterceptorHandler handler) async {
    final response = err.response;
    final message = response?.statusMessage?.toLowerCase() ?? "";

    // if (response?.statusCode == 401 || message.contains("unauthorize")) {
    //   navigator.currentContext?.sessionExpiredDialog(
    //     message: "Session Expired!",
    //     onPressed: () async {
    //       var id = getIt<UserCubit>().state.loginEntity?.profile?.id;

    //       getIt<UserCubit>().logOut();

    //       if (kIsWeb) {
    //         navigator.currentState?.context.go("/auth");
    //       } else {
    //         navigator.currentState
    //             ?.pushNamedAndRemoveUntil(PagePath.intro, (route) => false);

    //         await FirebaseMessaging.instance.unsubscribeFromTopic("$id");
    //       }
    //     },
    //   );
    // }

    handler.next(err);
  }
}
