import '../../../lib.dart';

class BaseApiResponseModel<T> {
  final String status;
  final int? totalResults;
  final T? data;
  final String message;

  const BaseApiResponseModel({
    required this.status,
    required this.totalResults,
    this.data,
    this.message = '',
  });

  factory BaseApiResponseModel.fromJson(
    Map<String, dynamic> json, {
    T? Function(dynamic response)? generateData,
  }) {
    if ((json['status'] as String? ?? "-1") != "ok") {
      throw RemoteException(message: json['message'] as String?);
    }

    return BaseApiResponseModel(
      status: json['status'] as String,
      totalResults:
          json['totalResults'] == null ? null : json['totalResults'] as int,
      data: generateData == null ? null : generateData(json['articles']),
    );
  }

  Map<String, dynamic> toJson(
    dynamic Function(T? data) generateJsonData,
  ) =>
      {
        'status': status,
        'totalResults': totalResults,
        'articles': generateJsonData(data),
      };
}
