class UrlConstant {
  /// API
  static const baseUrl = 'https://newsapi.org';

  static const getNews = '$baseUrl/v2/everything';
  static const categoryGeneral = '$baseUrl/v2/top-headlines';
  // static const apiKey = '130a1cf11d6a4d5ab02dc85c2bc4fe77';
  static const apiKey = 'c94306d0fa994d1b87a5ecc730b1095a';
}
